# -*- coding: utf-8 -*-
# encoding=utf8

from __future__ import unicode_literals

import sys
reload(sys)
sys.setdefaultencoding('utf8')

from django.core.paginator import Paginator
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from django.db.models import FloatField
from django.db.models.functions import Cast
from django.contrib.auth import authenticate, login

from slugify import Slugify

from .models import Translation, TransDict, TransLanguage, TransGroup, TransUser
from .forms import TransLanguageForm, TransGroupForm, TransDictForm, TranslationForm, LoginForm
from .decorators import user_login_required

import json

args = {}


def user_login(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = authenticate(username=username, password=password)
	url_redirect = request.GET.get('next', '')
	if len(url_redirect) >= 1:
		url_redirect = request.GET.get('next')
	else:
		url_redirect = "/"

	if user is not None:
		#		return HttpResponseRedirect(url_redirect)
		# the password verified for the user
		if user.is_active:
			login(request, user)
			return HttpResponseRedirect(url_redirect)
		else:
			return HttpResponseRedirect("/login/")
	else:
		return HttpResponseRedirect("/login/")


@user_login_required
def homepage(request):
	args['main_dictionary'] = main_dictionary = TransDict.objects.count()
	args['languages'] = languages = TransLanguage.objects.annotate(num_translation=Count('trans_language_key')).annotate(percent=Cast(Count('trans_language_key') * 100 / main_dictionary, FloatField())).order_by('id')
	return render(request, 'home.html', args)
	
	
@user_login_required
def translation_list(request, page_number=1):
	lang_to = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.language_code
	lang_from = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.language_code

	trans_to_list = Translation.objects.filter(
		trans_language__language_code__exact=lang_to,
	).values_list('trans_sentence_id', flat=True)

	if len(str(request.COOKIES.get('show_translations_by', ''))) >= 1:
		show_translations_by = request.COOKIES.get('show_translations_by')
	else:
		show_translations_by = 20

	all_translations = Translation.objects.filter(
		trans_language__language_code__exact=lang_from,
	).order_by('-id')

	args['filter_group'] = filter_group = request.COOKIES.get('filter_group', 'all')

	if len(str(filter_group)) >= 1:
		if filter_group == 'all':
			pass
		else:
			all_translations = all_translations.filter(trans_sentence__trans_dict_group__trans_group_alias=request.COOKIES.get('filter_group'))


	try:
		args['filter_untranslated'] = filter_untranslated = request.COOKIES.get('filter_untranslated')
	except:
		args['filter_untranslated'] = filter_untranslated = '0'
	if filter_untranslated == '1':
		all_translations = all_translations.exclude(trans_sentence_id__in=trans_to_list)
	else:
		pass

	translations_page = Paginator(all_translations, show_translations_by)
	try:
		args['translations'] = translations_page.page(page_number)
	except:
		args['translations'] = translations_page.page(1)
	args['trans_from'] = lang_from
	args['trans_to'] = lang_to
	args['languages'] = all_languages = TransLanguage.objects.all().order_by('id')
	args['main_dictionary'] = TransDict.objects.all()
	args['cookies'] = request.COOKIES
	args['show_translations_by'] = show_translations_by
	args['all_translations'] = Translation.objects.all().order_by('-id')
	args['all_groups'] = TransGroup.objects.all().order_by('id')
	args['lang_from_id'] = lang_from_id = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.id
	args['lang_to_id'] = lang_to_id = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.id

	if request.COOKIES.get('show_translations_by'):
		default = 0
		args['default'] = default
	else:
		default = 1
		args['default'] = default
	return render(request, 'translation_page.html', args)


@user_login_required
def translation_add(request):
	args['trans_to'] = lang_to = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.language_code
	args['trans_from'] = lang_from = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.language_code
	
	args['all_translations'] = Translation.objects.all().order_by('-id')
	args['all_groups'] = TransGroup.objects.all().order_by('id')
	if request.POST:
		translation = request.POST.get('trans_translation', '')
		if len(str(translation)) >= 1:
			pass
		else:
			HttpResponseRedirect('/translation/')

		lang_from_id = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.id
		lang_to_id = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.id
		try:
			translate = Translation.objects.get(
				trans_sentence_id__exact=request.POST.get('trans_sentence_id'),
				trans_language_id__exact=lang_to_id
			)
		except:
			translate = None

		if translate is not None:
			translate.trans_translation = request.POST.get('trans_translation')
			translate.save()
			args['translation'] = translate
			return render(request, 'tables/translation_tables/translation_row_ajax.html', args)
		else:
			translate_save_form = TranslationForm(data=request.POST)
			if translate_save_form.is_valid():
				translate = translate_save_form.save(commit=False)
				translate.trans_translation = request.POST.get('trans_translation')
				translate.trans_language_id = request.POST.get('trans_language_id')
				translate.trans_sentence_id = request.POST.get('trans_sentence_id')
				translate.save()
				args['translation'] = translate
				return render(request, 'tables/translation_tables/translation_row_ajax.html', args)
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/translation/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/translation/' + message + msg_txt)


@user_login_required
def translation_search_ajax(request):
	args['trans_to'] = lang_to = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.language_code
	args['trans_from'] = lang_from = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.language_code
	args['system_value'] = TransDict.objects.filter(trans_dict_sentence__contains=request.POST.get("search-system-values")).values_list('id')
	args['translations'] = Translation.objects.filter(
		trans_sentence_id__in=args['system_value'],
		trans_language__language_code__exact=lang_to
	)[0:50]
	return render(request, 'tables/translation_tables/translation_search_ajax.html', args)


@user_login_required
def translation_search_translated_ajax(request):
	args['trans_to'] = lang_to = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_to.language_code
	args['trans_from'] = lang_from = TransUser.objects.get(trans_user_id=request.user.id).trans_translate_from.language_code
	args['translations'] = Translation.objects.filter(
		trans_translation__icontains=request.POST.get("search-translated"),
		trans_language__language_code__exact=lang_to
	)[0:50]
	return render(request, 'tables/translation_tables/translation_search_translated_ajax.html', args)


@user_login_required
def translation_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/translation/')
			response.set_cookie('show_translations_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/translation/')
	else:
		return HttpResponseRedirect('/translation/')


@user_login_required
def translation_change_show_group(request):
	if request.POST:
		show_group = request.POST.get('show_by_group', None)
		if show_group is not None:
			response = HttpResponseRedirect('/translation/')
			response.set_cookie('filter_group', show_group)
			return response
		else:
			return HttpResponseRedirect('/translation/')
	else:
		return HttpResponseRedirect('/translation/')


@user_login_required
def translation_change_show_untranslated(request):
	if request.POST:
		show_untranslated = request.POST.get('untranslated')
		response = HttpResponseRedirect('/translation/')

		if show_untranslated == 'on':
			try:
				response.set_cookie('filter_untranslated', show_untranslated)
				return response
			except:
				pass
		else:
			try:
				response.set_cookie('filter_untranslated', show_untranslated)
				return response
			except:
				pass
		return HttpResponseRedirect('/translation/')
	else:
		return HttpResponseRedirect('/translation/')


@user_login_required
def language_add(request):
	if request.POST:
		try:
			language = TransLanguage.objects.get(
				id__exact=request.POST.get('language_id')
			)
		except:
			language = None

		if language is not None:
			language.language_name = request.POST.get('language_name')
			language.language_native = request.POST.get('language_native')
			language.language_code = request.POST.get('language_code')
			language.save()
			return HttpResponseRedirect('/languages/')
		else:
			language_save_form = TransLanguageForm(data=request.POST)
			if language_save_form.is_valid():
				language = language_save_form.save(commit=False)
				language.language_name = request.POST.get('language_name')
				language.language_native = request.POST.get('language_native')
				language.language_code = request.POST.get('language_code')
				language.save()
				return HttpResponseRedirect('/languages/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/languages/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/languages/' + message + msg_txt)


@user_login_required
def language_manager_list(request):
	args['languages'] = TransLanguage.objects.all().order_by('id')
	return render(request, 'languages_manager.html', args)


@user_login_required
def language_delete(request):
	if request.POST:
		try:
			language_id = request.POST.get('language_delete')
			language = TransLanguage.objects.get(id=language_id)
			language.delete()
			return HttpResponseRedirect('/languages/')
		except ObjectDoesNotExist:
			language = None
			return HttpResponseRedirect('/languages/')
	else:
		return HttpResponseRedirect('/languages/')


@user_login_required
def group_add(request):
	if request.POST:
		try:
			group = TransGroup.objects.get(
				id__exact=request.POST.get('group_id')
			)
		except:
			group = None

		if group is not None:
			group.trans_group_title = request.POST.get('group_name')
			group.trans_group_alias = request.POST.get('group_alias')
			group.save()
			return HttpResponseRedirect('/groups/')
		else:
			group_save_form = TransGroupForm(data=request.POST)
			if group_save_form.is_valid():
				group = group_save_form.save(commit=False)
				group.trans_group_title = request.POST.get('group_name')
				group.trans_group_alias = request.POST.get('group_alias')
				group.save()
				return HttpResponseRedirect('/groups/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/groups/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/groups/' + message + msg_txt)


@user_login_required
def group_manager_list(request):
	args['groups'] = TransGroup.objects.all().order_by('id')
	return render(request, 'groups_manager.html', args)


@user_login_required
def group_delete(request):
	if request.POST:
		try:
			group_id = request.POST.get('group_delete')
			group = TransGroup.objects.get(id=group_id)
			group.delete()
			return HttpResponseRedirect('/groups/')
		except ObjectDoesNotExist:
			group = None
			return HttpResponseRedirect('/groups/')
	else:
		return HttpResponseRedirect('/groups/')


@user_login_required
def main_dictionary_add(request):
	if request.POST:
		try:
			main_dictionary = TransDict.objects.get(
				id__exact=request.POST.get('main_dictionary_id')
			)
		except:
			main_dictionary = None

		if main_dictionary is not None:
			main_dictionary.trans_dict_sentence = request.POST.get('sentence')
			main_dictionary.trans_dict_group_id = request.POST.get('group_alias')
			main_dictionary.save()

			try:
				assigned_english = Translation.objects.get(
					trans_sentence_id__exact=request.POST.get('main_dictionary_id'),
					id__exact=request.POST.get('english_translation_id')
				)
			except:
				assigned_english = None

			if assigned_english is not None:
				main_english = assigned_english
				main_english.trans_translation = request.POST.get('english_translation')
				main_english.save()
				return HttpResponseRedirect('/main_dictionary/')
			else:
				main_english_save_form = TranslationForm(data=request.POST)
				if main_english_save_form.is_valid():
					main_english = main_english_save_form.save(commit=False)
					main_english.trans_language_id = '1'
					main_english.trans_translation = request.POST.get('english_translation')
					main_english.trans_sentence_id = main_dictionary.id
					main_english.save()
					return HttpResponseRedirect('/main_dictionary/')
		else:
			main_dictionary_save_form = TransDictForm(data=request.POST)
			if main_dictionary_save_form.is_valid():
				main_dictionary = main_dictionary_save_form.save(commit=False)
				main_dictionary.trans_dict_sentence = request.POST.get('sentence')
				main_dictionary.trans_dict_group_id = request.POST.get('group_alias')
				main_dictionary.save()

				try:
					main_dictionary = TransDict.objects.get(
						id__exact=main_dictionary.id
					)
				except:
					main_dictionary = None

				if main_dictionary is not None:
					main_english_save_form = TranslationForm(data=request.POST)
					if main_english_save_form.is_valid():
						main_english = main_english_save_form.save(commit=False)
						main_english.trans_language_id = '1'
						main_english.trans_translation = request.POST.get('english_translation')
						main_english.trans_sentence_id = main_dictionary.id
						main_english.save()
						return HttpResponseRedirect('/main_dictionary/')
				else:
					pass

				return HttpResponseRedirect('/main_dictionary/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/main_dictionary/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/main_dictionary/' + message + msg_txt)


@csrf_exempt
def post(request):
	response = {}
	if request.POST:
		domain = request.POST.get('domain')
		slug = request.POST.get('slug')
		title = request.POST.get('title')
		try:
			main_dictionary = TransDict.objects.get(
				trans_dict_sentence__exact=slug,
				trans_dict_group_id__exact=domain
			)
		except:
			main_dictionary = None
			
		if main_dictionary is None:
			main_dictionary_save_form = TransDictForm(data=request.POST)
			if main_dictionary_save_form.is_valid():
				main_dictionary = main_dictionary_save_form.save(commit=False)
				main_dictionary.trans_dict_sentence = slug
				main_dictionary.trans_dict_group_id = domain
				main_dictionary.save()
				
				try:
					main_dictionary = TransDict.objects.get(
						id__exact=main_dictionary.id
					)
				except:
					main_dictionary = None
				
				if main_dictionary is not None:
					main_english_save_form = TranslationForm(data=request.POST)
					if main_english_save_form.is_valid():
						main_english = main_english_save_form.save(commit=False)
						main_english.trans_language_id = '1'
						main_english.trans_translation = title
						main_english.trans_sentence_id = main_dictionary.id
						main_english.save()
						response['success'] = 'true'
						response['found'] = 0
						response['added'] = 1
						return HttpResponse(json.dumps(response), content_type="application/json")
				else:
					pass
		else:
			response['success'] = 'true'
			response['found'] = 1
			response['added'] = 0
			return HttpResponse(json.dumps(response), content_type="application/json")
	else:
		response['success'] = 'false'
		response['message'] = 'Wrong request method.'
		return HttpResponse(json.dumps(response), content_type="application/json")
	

@csrf_exempt
def delete_access_group_item(request):
	response = {}
	if request.POST:
		domain = request.POST.get('domain')
		slug = request.POST.get('slug')
		try:
			main_dictionary = TransDict.objects.get(
				trans_dict_sentence__exact=slug,
				trans_dict_group_id__exact=domain
			)
		except:
			main_dictionary = None
		if main_dictionary is not None:
			main_dictionary.delete()
			response['success'] = 'true'
			response['found'] = 1
			response['deleted'] = 1
			return HttpResponse(json.dumps(response), content_type="application/json")
		else:
			main_dictionary = None
			response['success'] = 'true'
			response['found'] = 0
			response['deleted'] = 0
			return HttpResponse(json.dumps(response), content_type="application/json")
	else:
		response['success'] = 'false'
		response['message'] = 'Wrong request method.'
		return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def add_access_item(request):
	response = {}
	if request.POST:
		domain = request.POST.get('domain')
		slug = request.POST.get('slug')
		title = request.POST.get('title')
		try:
			main_dictionary = TransDict.objects.get(
				trans_dict_sentence__exact=slug,
				trans_dict_group_id__exact=domain
			)
		except:
			main_dictionary = None
			
		if main_dictionary is None:
			main_dictionary_save_form = TransDictForm(data=request.POST)
			if main_dictionary_save_form.is_valid():
				main_dictionary = main_dictionary_save_form.save(commit=False)
				main_dictionary.trans_dict_sentence = slug
				main_dictionary.trans_dict_group_id = domain
				main_dictionary.save()
				
				try:
					main_dictionary = TransDict.objects.get(
						id__exact=main_dictionary.id
					)
				except:
					main_dictionary = None
				
				if main_dictionary is not None:
					main_english_save_form = TranslationForm(data=request.POST)
					if main_english_save_form.is_valid():
						main_english = main_english_save_form.save(commit=False)
						main_english.trans_language_id = '1'
						main_english.trans_translation = title
						main_english.trans_sentence_id = main_dictionary.id
						main_english.save()
						response['success'] = 'true'
						response['found'] = 0
						response['added'] = 1
						return HttpResponse(json.dumps(response), content_type="application/json")
				else:
					pass
		else:
			response['success'] = 'true'
			response['found'] = 1
			response['added'] = 0
			return HttpResponse(json.dumps(response), content_type="application/json")
	else:
		response['success'] = 'false'
		response['message'] = 'Wrong request method.'
		return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def delete_access_item(request):
	response = {}
	if request.POST:
		domain = request.POST.get('domain')
		slug = request.POST.get('slug')
		try:
			main_dictionary = TransDict.objects.get(
				trans_dict_sentence__exact=slug,
				trans_dict_group_id__exact=domain
			)
		except:
			main_dictionary = None
		if main_dictionary is not None:
			main_dictionary.delete()
			response['success'] = 'true'
			response['found'] = 1
			response['deleted'] = 1
			return HttpResponse(json.dumps(response), content_type="application/json")
		else:
			main_dictionary = None
			response['success'] = 'true'
			response['found'] = 0
			response['deleted'] = 0
			return HttpResponse(json.dumps(response), content_type="application/json")
	else:
		response['success'] = 'false'
		response['message'] = 'Wrong request method.'
		return HttpResponse(json.dumps(response), content_type="application/json")
		
	
@user_login_required
def system_value_available(request):
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('system_value'):
			system_value_str = get['system_value']
			system_value_group_str = get['trans_dict_group']
			if TransDict.objects.filter(
					trans_dict_sentence=system_value_str,
					trans_dict_group_id=system_value_group_str
			).count() == 0:
				return HttpResponse(str(system_value_str))
			else:
				return HttpResponseServerError(system_value_str)
	return HttpResponseServerError("Requires a slug field.")


@user_login_required
def slugify_function(request):
	error_msg = u"No GET data sent."
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('english_translation'):
			english_translation = get['english_translation']
			slug = Slugify(to_lower=True, separator='_')
			system_value_slugify = slug(english_translation)
			return HttpResponse(system_value_slugify)
		else:
			error_msg = u"Insufficient GET data (need 'slug' and 'title'!)"
	return HttpResponseServerError(error_msg)


@user_login_required
def main_dictionary_list(request, page_number=1):
	if len(str(request.COOKIES.get('show_main_dictionary_by', ''))) >= 1:
		show_main_dictionary_by = request.COOKIES.get('show_main_dictionary_by')
	else:
		show_main_dictionary_by = 20

	args['main_dictionary'] = main_dictionary = TransDict.objects.all().order_by('-id')
	main_dictionary_page = Paginator(main_dictionary, show_main_dictionary_by)
	try:
		args['main_dictionary_values'] = main_dictionary_page.page(page_number)
	except:
		args['main_dictionary_values'] = main_dictionary_page.page(1)
	args['groups'] = TransGroup.objects.all()
	return render(request, 'main_dictionary_manager.html', args)


@user_login_required
def main_dictionary_change_show_count(request):
	if request.POST:
		show_count = request.POST.get('show_by_count', None)
		if show_count is not None:
			response = HttpResponseRedirect('/main_dictionary/')
			response.set_cookie('show_main_dictionary_by', show_count)
			return response
		else:
			return HttpResponseRedirect('/main_dictionary/')
	else:
		return HttpResponseRedirect('/main_dictionary/')


@csrf_exempt
def test(request):
	filename = "static/language_dictionary.py"
	text_file = open(filename, 'w+')

	default_paragraph = str(
		"# -*- coding:utf-8 -*-" + '\n' +

		"from __future__ import unicode_literals" + '\n\n' +

		"from django import template" + '\n' +
		"from ecela import auth_settings" + '\n' +

		"register = template.Library()" + '\n\n\n'
	)

	text_file.write(default_paragraph)

	all_languages = TransLanguage.objects.all().exclude(language_code='en')
	all_trans_system_values = Translation.objects.filter(trans_language__language_code="en")

	for item in all_trans_system_values:

		#	trans_system_value_alias = str(trans_system_value.trans_dict_sentence)
		#	trans_dict_domain = str(trans_system_value.trans_dict_group.trans_group_alias)

		trans_english = item.trans_translation

		text_to_write = str(
			"@register.simple_tag" + "\n" +
			"def " + str(item.trans_sentence.trans_dict_group.trans_group_alias) + "_" +
			str(item.trans_sentence.trans_dict_sentence) + "(lang=None):" + '\n' +
			'\t' + "if lang == 'en':" + "\n" +
			"\t\t" + "content = " + '"' + str(item.trans_translation) + '"' + '\n'
		)

		for language in all_languages:
			try:
				lang_items = Translation.objects.filter(trans_language__language_code=language.language_code)
			except:
				lang_items = None

			for extra_item in lang_items:

				if extra_item.trans_sentence_id == item.trans_sentence_id:

					extra_item_string = str(
						"\t" + "elif lang == " + "'" + str(extra_item.trans_language.language_code) + "':" + "\n" +
						"\t\t" + "content = " + '"' + u''.join(extra_item.trans_translation).strip() + '"' + "\n"
					)

					text_to_write = str(text_to_write) + str(extra_item_string)
				else:
					pass

		default_english = str(
			"\t" + "else:" + "\n" +
			"\t\t" + "content = " + '"' + str(item.trans_translation) + '"' + '\n'
		)

		finalize_function = str(
			"\t" + "return content" + "\n\n\n"
		)

		text_to_write = str(text_to_write) + str(default_english) + str(finalize_function)

		text_file.write(str(text_to_write))

	text_file.close()

	return HttpResponseRedirect("/")


@csrf_exempt
def get_translations(request):
	params = {}
	params['success'] = "false"
	translation_list = []
	if request.POST:
		if 'trans_english' in request.POST:
			try:
				translation_system_value = Translation.objects.get(
					trans_language__language_code="en",
					trans_translation=str(request.POST.get('trans_english'))
				)
			except:
				translation_system_value = None
			if translation_system_value is not None:
				translation_languages = TransLanguage.objects.all()
				for language in translation_languages:
					try:
						translation = Translation.objects.get(
							trans_language__language_code=str(language.language_code),
							trans_sentence__trans_dict_sentence=translation_system_value.trans_sentence.trans_dict_sentence,
						)
					except:
						translation = None
					if translation is not None:
						translation_list.append({
							'trans_system_value': translation_system_value.trans_sentence.trans_dict_sentence,
							'trans_system_value_id': translation_system_value.trans_sentence.id,
							'trans_language_code': language.language_code,
							'trans_language_name': language.language_name,
							'trans_language_id': language.id,
							'translation': translation.trans_translation,
							'translation_id': translation.id,
						})
					else:
						translation_list.append({
							'trans_system_value': translation_system_value.trans_sentence.trans_dict_sentence,
							'trans_system_value_id': translation_system_value.trans_sentence.id,
							'trans_language_code': language.language_code,
							'trans_language_name': language.language_name,
							'trans_language_id': language.id,
							'translation': "",
							'translation_id': "",
						})
				params['translations'] = translation_list
				params['success'] = "true"
				return HttpResponse(json.dumps(params), content_type="application/json")
			else:
				group = TransGroup.objects.get(
						trans_group_alias__exact=str(request.POST.get('trans_group'))
					)
				main_dictionary_save_form = TransDictForm(data=request.POST)
				if main_dictionary_save_form.is_valid():
					main_dictionary = main_dictionary_save_form.save(commit=False)
					slug = Slugify(to_lower=True, separator='_')
					system_value_slugify = slug(str(request.POST.get('trans_english'))) + '_' + str(request.POST.get('id'))
					main_dictionary.trans_dict_sentence = system_value_slugify
					main_dictionary.trans_dict_group_id = group.id
					main_dictionary.save()

					try:
						main_dictionary = TransDict.objects.get(
							id__exact=main_dictionary.id
						)
					except:
						main_dictionary = None

					if main_dictionary is not None:
						main_english_save_form = TranslationForm(data=request.POST)
						if main_english_save_form.is_valid():
							main_english = main_english_save_form.save(commit=False)
							main_english.trans_language_id = '1'
							main_english.trans_translation = str(request.POST.get('trans_english'))
							main_english.trans_sentence_id = main_dictionary.id
							main_english.save()
							translation_languages = TransLanguage.objects.all()
							for language in translation_languages:
								if language.language_code == "en":
									translation_list.append({
										'trans_system_value': str(main_dictionary.trans_dict_sentence),
										'trans_system_value_id': main_dictionary.id,
										'trans_language_code': language.language_code,
										'trans_language_name': language.language_name,
										'trans_language_id': language.id,
										'translation': main_english.trans_translation,
										'translation_id': main_english.id,
									})
								else:
									translation_list.append({
										'trans_system_value': str(main_dictionary.trans_dict_sentence),
										'trans_system_value_id': main_dictionary.id,
										'trans_language_code': language.language_code,
										'trans_language_name': language.language_name,
										'trans_language_id': language.id,
										'translation': "",
										'translation_id': "",
									})
							params['translations'] = translation_list
							params['success'] = "true"
							return HttpResponse(json.dumps(params), content_type="application/json")
						else:
							params['message'] = "Main english form is not valid"
							return HttpResponse(json.dumps(params), content_type="application/json")
					else:
						params['message'] = "There is no 'main_english' found"
						return HttpResponse(json.dumps(params), content_type="application/json")
				else:
					params['message'] = "Main dictionary form is not valid"
					return HttpResponse(json.dumps(params), content_type="application/json")
		else:
			params['message'] = "Wrong request name param. Try: 'trans_english'."
			return HttpResponse(json.dumps(params), content_type="application/json")
	else:
		params['message'] = "Wrong request method. Try only POST form-encoded method!"
		return HttpResponse(json.dumps(params), content_type="application/json")


@csrf_exempt
def save_translations(request):
	params = {}
	params['success'] = "false"
	if request.POST:
		request_conditions = ['translation' in request.POST, 'translation_id' in request.POST, 'trans_language_id' in request.POST, 'trans_system_value_id' in request.POST]
		if all(request_conditions):
			try:
				translate = Translation.objects.get(
					id=int(request.POST.get('translation_id'))
				)
			except:
				translate = None
			if translate is not None:
				translate.trans_translation = str(request.POST.get('translation'))
				translate.save()
				params['translation'] = translate.trans_translation
				params['success'] = "true"
				return HttpResponse(json.dumps(params), content_type="application/json")
			else:
				translate_save_form = TranslationForm(data=request.POST)
				if translate_save_form.is_valid():
					translate = translate_save_form.save(commit=False)
					translate.trans_sentence_id = int(request.POST.get('trans_system_value_id'))
					translate.trans_language_id = int(request.POST.get('trans_language_id'))
					translate.trans_translation = str(request.POST.get('translation'))
					translate.save()
					params['translation'] = translate.trans_translation
					params['success'] = "true"
					return HttpResponse(json.dumps(params), content_type="application/json")
		else:
			params['message'] = "Wrong request name params."
			return HttpResponse(json.dumps(params), content_type="application/json")
	else:
		params['message'] = "Wrong request method. Try only POST form-encoded method!"
		return HttpResponse(json.dumps(params), content_type="application/json")


@csrf_exempt
def export_json(request):
	params = {}
	params['success'] = 'false'
	try:
		all_languages = TransLanguage.objects.all()
		dictionary_json_files = []
		for language in all_languages:
			with open('static/dictionaries/json_' + language.language_code + '.json', 'wb') as json_file:
				#translations_list = []
				#translations = Translation.objects.filter(trans_language_id=language.id)
				#for translation in translations:
				#	translations_list.append({
				#		'id': translation.id,
				#		'domain': str(translation.trans_sentence.trans_dict_group.trans_group_alias),
				#		'alias': str(translation.trans_sentence.trans_dict_sentence),
				#		'translation': str(translation.trans_translation),
				#		'system_id': str(translation.trans_system_id) if translation.trans_system_id is not None else None,
				#	})
				#result = json.dumps(translations_list, ensure_ascii=False)

				all_system_values = TransDict.objects.all()
				translations_list = []
				for system_value in all_system_values:
					try:
						translation = Translation.objects.get(trans_sentence_id=system_value.id, trans_language_id=language.id)
					except:
						translation = None
					if translation is not None:
						translations_list.append({
							'id': translation.id,
							'domain': str(translation.trans_sentence.trans_dict_group.trans_group_alias),
							'alias': str(translation.trans_sentence.trans_dict_sentence),
							'translation': str(translation.trans_translation),
							'system_id': translation.trans_system_id if translation.trans_system_id is not None else "",
						})
					else:
						default_translation = Translation.objects.get(trans_sentence_id=system_value.id, trans_language_id__language_code="en")
						translations_list.append({
							'id': default_translation.id,
							'domain': str(default_translation.trans_sentence.trans_dict_group.trans_group_alias),
							'alias': str(default_translation.trans_sentence.trans_dict_sentence),
							'translation': str(default_translation.trans_translation),
							'system_id': default_translation.trans_system_id if default_translation.trans_system_id is not None else "",
						})
				result = json.dumps(translations_list, ensure_ascii=False)
				json_file.write(result)
			dictionary_json_files.append(
				{
					language.language_name: 'json_' + language.language_code + '.json',
				}
			)
		params['success'] = 'true'
		params['dictionary_json_files'] = dictionary_json_files
		return HttpResponse(json.dumps(params), content_type="application/json")
	except:
		params['msg_text'] = "No language found"
		return HttpResponse(json.dumps(params), content_type="application/json")


@csrf_exempt
def get_translation_dictionary_by_lang_code(request):
	params={}
	params['success'] = "false"
	
	try:
		lang_code = str(request.POST.get('lang_code')).lower()
	except:
		params['msg_text'] = 'Wrong lang_code'
		return HttpResponse(json.dumps(params), content_type="application/json")
	
	try:
		with open('static/dictionaries/json_' + lang_code + '.json', 'r') as json_dictionary:
			params['dictionary'] = json.load(json_dictionary)
			params['success'] = "true"
			return HttpResponse(json.dumps(params), content_type="application/json")
	except:
		params['msg_text'] = 'Error while reading file'
		return HttpResponse(json.dumps(params), content_type="application/json")
