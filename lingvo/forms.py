# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from .models import TransLanguage, TransGroup, TransDict, Translation


class TransLanguageForm(forms.ModelForm):
    class Meta:
        model = TransLanguage
        exclude = (
            'language_name',
            'language_native',
            'language_code'
        )


class TransGroupForm(forms.ModelForm):
    class Meta:
        model = TransGroup
        exclude = (
            'trans_group_title',
            'trans_group_alias'
        )


class TransDictForm(forms.ModelForm):
    class Meta:
        model = TransDict
        exclude = (
            'trans_dict_sentence',
            'trans_dict_group'
        )


class TranslationForm(forms.ModelForm):
    class Meta:
        model = Translation
        exclude = (
            'trans_sentence',
            'trans_language',
            'trans_translation'
        )


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)