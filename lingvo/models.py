# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class TransLanguage(models.Model):
    language_name = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    language_code = models.CharField(
        max_length=2,
        blank=False,
        null=False
    )

    language_native = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    def __str__(self):
        return str(str(self.language_name) + ' (' + str( self.language_code) + ')')


class TransGroup(models.Model):
    trans_group_title = models.CharField(
        max_length=200,
        blank=False,
        null=False
    )

    trans_group_alias = models.CharField(
        max_length=200,
        blank=False,
        null=False
    )


class TransDict(models.Model):
    trans_dict_sentence = models.TextField(
        blank=True,
        null=True
    )

    trans_dict_group = models.ForeignKey(
        "TransGroup",
        related_name="trans_dict_group_key",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )


class Translation(models.Model):
    trans_sentence = models.ForeignKey(
        "TransDict",
        related_name="trans_sentence_key",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )
    trans_language = models.ForeignKey(
        "TransLanguage",
        related_name="trans_language_key",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )

    trans_translation = models.TextField(
        blank=True,
        null=True
    )
    trans_system_id = models.IntegerField(
        blank=True,
        null=True,
    )


class TransUser(models.Model):
    trans_user = models.ForeignKey(
        User,
        related_name="trans_user_key",
        on_delete=models.CASCADE,
        blank=False,
        null=False
    )
    trans_translate_from = models.ForeignKey(
        "TransLanguage",
        related_name="trans_translate_from_key",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    trans_translate_to = models.ForeignKey(
        "TransLanguage",
        related_name="trans_translate_to_key",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    def __str__(self):
        return str(str(self.trans_user) + ' (' + str( self.trans_translate_from.language_name) + ' - ' + str( self.trans_translate_to.language_name) + ')')
