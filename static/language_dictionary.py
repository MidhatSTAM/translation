# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django import template
from ecela import auth_settings
register = template.Library()


@register.simple_tag
def general_company(lang=None):
	if lang == 'en':
		content = "Company"
	elif lang == 'tr':
		content = "Şirketim"
	elif lang == 'bs':
		content = "Firma"
	elif lang == 'ru':
		content = "Компания"
	else:
		content = "Company"
	return content


@register.simple_tag
def general_dashboard(lang=None):
	if lang == 'en':
		content = "Dashboard"
	elif lang == 'tr':
		content = "Anasayfa"
	elif lang == 'bs':
		content = "Kontrolna ploča"
	elif lang == 'ru':
		content = "Панель управления"
	else:
		content = "Dashboard"
	return content


@register.simple_tag
def general_branches(lang=None):
	if lang == 'en':
		content = "Branches"
	elif lang == 'tr':
		content = "Şubeler"
	elif lang == 'bs':
		content = "Filijala"
	elif lang == 'ru':
		content = "Филиалы"
	else:
		content = "Branches"
	return content


@register.simple_tag
def general_main_menu(lang=None):
	if lang == 'en':
		content = "Main menu"
	elif lang == 'tr':
		content = "Ana menü"
	elif lang == 'bs':
		content = "Glavni meni"
	elif lang == 'ru':
		content = "Главное меню"
	else:
		content = "Main menu"
	return content


@register.simple_tag
def general_forgot_pass_ques(lang=None):
	if lang == 'en':
		content = "Forgot your password"
	elif lang == 'tr':
		content = "Parolanızı unuttunuz mu?"
	elif lang == 'bs':
		content = "Izgubljena šifra"
	elif lang == 'ru':
		content = "Забыли свой пароль?"
	else:
		content = "Forgot your password"
	return content


@register.simple_tag
def general_my_profile(lang=None):
	if lang == 'en':
		content = "My profile"
	elif lang == 'tr':
		content = "Profilim"
	elif lang == 'bs':
		content = "Moj profil"
	elif lang == 'ru':
		content = "Мой профиль"
	else:
		content = "My profile"
	return content


@register.simple_tag
def general_logout(lang=None):
	if lang == 'en':
		content = "Logout"
	elif lang == 'tr':
		content = "Çıkış yap"
	elif lang == 'bs':
		content = "Odjava"
	elif lang == 'ru':
		content = "Выйти"
	else:
		content = "Logout"
	return content


@register.simple_tag
def general_login_to_your_account(lang=None):
	if lang == 'en':
		content = "Login to your account"
	elif lang == 'tr':
		content = "Hesabınıza giriş yapın"
	elif lang == 'bs':
		content = "Prijavi se"
	elif lang == 'ru':
		content = "Войдите в аккаунт"
	else:
		content = "Login to your account"
	return content


@register.simple_tag
def general_password(lang=None):
	if lang == 'en':
		content = "Password"
	elif lang == 'tr':
		content = "Parola"
	elif lang == 'bs':
		content = "Šifra"
	elif lang == 'ru':
		content = "Пароль"
	else:
		content = "Password"
	return content


@register.simple_tag
def general_sign_in(lang=None):
	if lang == 'en':
		content = "Sign in"
	elif lang == 'tr':
		content = "Giriş yap"
	elif lang == 'bs':
		content = "Prijavi se"
	elif lang == 'ru':
		content = "Войти"
	else:
		content = "Sign in"
	return content


@register.simple_tag
def general_dont_have_account_quest(lang=None):
	if lang == 'en':
		content = "Don't have account?"
	elif lang == 'tr':
		content = "Hesabınız yok mu?"
	elif lang == 'bs':
		content = "Nemate račun?"
	elif lang == 'ru':
		content = "У вас нет аккаунта?"
	else:
		content = "Don't have account?"
	return content


@register.simple_tag
def general_sign_up(lang=None):
	if lang == 'en':
		content = "Sign up"
	elif lang == 'tr':
		content = "Kayıt ol"
	elif lang == 'bs':
		content = "Izradi račun"
	elif lang == 'ru':
		content = "Зарегистрироваться"
	else:
		content = "Sign up"
	return content


@register.simple_tag
def general_status_online(lang=None):
	if lang == 'en':
		content = "Online"
	elif lang == 'tr':
		content = "Çevrim içi"
	elif lang == 'bs':
		content = "Online"
	elif lang == 'ru':
		content = "В сети"
	else:
		content = "Online"
	return content


@register.simple_tag
def general_status_offline(lang=None):
	if lang == 'en':
		content = "Offline"
	elif lang == 'tr':
		content = "Çevrim dışı"
	elif lang == 'bs':
		content = "Offline"
	elif lang == 'ru':
		content = "Не в сети"
	else:
		content = "Offline"
	return content


@register.simple_tag
def general_status_away(lang=None):
	if lang == 'en':
		content = "Away"
	elif lang == 'tr':
		content = "Uzakta"
	elif lang == 'bs':
		content = "Vani"
	elif lang == 'ru':
		content = "Отсутствую"
	else:
		content = "Away"
	return content


@register.simple_tag
def general_analytics(lang=None):
	if lang == 'en':
		content = "Analytics"
	elif lang == 'tr':
		content = "İstatistik"
	elif lang == 'bs':
		content = "Statistika"
	elif lang == 'ru':
		content = "Статистика"
	else:
		content = "Analytics"
	return content


@register.simple_tag
def general_accessibility(lang=None):
	if lang == 'en':
		content = "Accessibility"
	elif lang == 'tr':
		content = "Erişim"
	elif lang == 'bs':
		content = "Pristupačnost"
	elif lang == 'ru':
		content = "Доступ"
	else:
		content = "Accessibility"
	return content


@register.simple_tag
def general_support(lang=None):
	if lang == 'en':
		content = "Support"
	elif lang == 'tr':
		content = "Destek"
	elif lang == 'bs':
		content = "Podrška"
	elif lang == 'ru':
		content = "Поддержка"
	else:
		content = "Support"
	return content


@register.simple_tag
def general_all_settings(lang=None):
	if lang == 'en':
		content = "All Settings"
	elif lang == 'tr':
		content = "Tüm ayarlar"
	elif lang == 'bs':
		content = "Sve postavke"
	elif lang == 'ru':
		content = "Все Настройки"
	else:
		content = "All Settings"
	return content


@register.simple_tag
def general_settings(lang=None):
	if lang == 'en':
		content = "Settings"
	elif lang == 'tr':
		content = "Ayarlar"
	elif lang == 'bs':
		content = "Postavke"
	elif lang == 'ru':
		content = "Настройки"
	else:
		content = "Settings"
	return content


@register.simple_tag
def general_connections(lang=None):
	if lang == 'en':
		content = "Connections"
	elif lang == 'tr':
		content = "Bağlantılar"
	elif lang == 'bs':
		content = "Veze"
	elif lang == 'ru':
		content = "Соединения"
	else:
		content = "Connections"
	return content


@register.simple_tag
def general_invoices(lang=None):
	if lang == 'en':
		content = "Invoices"
	elif lang == 'tr':
		content = "İnvoyslar"
	elif lang == 'bs':
		content = "Fakture"
	elif lang == 'ru':
		content = "Счета"
	else:
		content = "Invoices"
	return content


@register.simple_tag
def general_schedule(lang=None):
	if lang == 'en':
		content = "Schedule"
	elif lang == 'tr':
		content = "Görevler"
	elif lang == 'bs':
		content = "Raspored"
	elif lang == 'ru':
		content = "Расписание"
	else:
		content = "Schedule"
	return content


@register.simple_tag
def general_not_specified(lang=None):
	if lang == 'en':
		content = "Not specified"
	elif lang == 'tr':
		content = "Belirtilmemiş"
	elif lang == 'bs':
		content = "Neodređeno"
	elif lang == 'ru':
		content = "Не указано"
	else:
		content = "Not specified"
	return content


@register.simple_tag
def general_navigation(lang=None):
	if lang == 'en':
		content = "Navigation"
	elif lang == 'tr':
		content = "Bölümler"
	elif lang == 'bs':
		content = "Navigacija"
	elif lang == 'ru':
		content = "Разделы"
	else:
		content = "Navigation"
	return content


@register.simple_tag
def general_email(lang=None):
	if lang == 'en':
		content = "E-mail"
	elif lang == 'tr':
		content = "E-mail"
	elif lang == 'bs':
		content = "E-mail"
	elif lang == 'ru':
		content = "E-mail"
	else:
		content = "E-mail"
	return content


@register.simple_tag
def general_account_security(lang=None):
	if lang == 'en':
		content = "Account security"
	elif lang == 'tr':
		content = "Hesap güvenliği"
	elif lang == 'bs':
		content = "Sigurnost računa"
	elif lang == 'ru':
		content = "Защита аккаунта"
	else:
		content = "Account security"
	return content


@register.simple_tag
def general_username(lang=None):
	if lang == 'en':
		content = "Username"
	elif lang == 'tr':
		content = "Kullanıcı adı"
	elif lang == 'bs':
		content = "Korisničko ime"
	elif lang == 'ru':
		content = "Имя пользователя"
	else:
		content = "Username"
	return content


@register.simple_tag
def general_messages(lang=None):
	if lang == 'en':
		content = "Messages"
	elif lang == 'tr':
		content = "Mesajlar"
	elif lang == 'bs':
		content = "Poruke"
	elif lang == 'ru':
		content = "Сообщения"
	else:
		content = "Messages"
	return content


@register.simple_tag
def general_remember_me(lang=None):
	if lang == 'en':
		content = "Remember me"
	elif lang == 'tr':
		content = "Beni hatırla"
	elif lang == 'bs':
		content = "Zapamti me"
	elif lang == 'ru':
		content = "Запомнить меня"
	else:
		content = "Remember me"
	return content


@register.simple_tag
def profile_user_profile(lang=None):
	if lang == 'en':
		content = "User profile"
	elif lang == 'tr':
		content = "Kullanıcı profili"
	elif lang == 'bs':
		content = "Korisnički profil"
	elif lang == 'ru':
		content = "Профиль пользователя"
	else:
		content = "User profile"
	return content


@register.simple_tag
def profile_my_user_profile(lang=None):
	if lang == 'en':
		content = "My profile"
	elif lang == 'tr':
		content = "Kullanıcı profilim"
	elif lang == 'bs':
		content = "Moj profil"
	elif lang == 'ru':
		content = "Мой профиль"
	else:
		content = "My profile"
	return content


@register.simple_tag
def profile_profile_overview(lang=None):
	if lang == 'en':
		content = "Profile overview"
	elif lang == 'tr':
		content = "Profil bakışı"
	elif lang == 'bs':
		content = "Pregled profila"
	elif lang == 'ru':
		content = "Обзор профиля"
	else:
		content = "Profile overview"
	return content


@register.simple_tag
def profile_personal_information(lang=None):
	if lang == 'en':
		content = "Personal information"
	elif lang == 'tr':
		content = "Şahıs bilgilerim"
	elif lang == 'bs':
		content = "Korisničke informacije"
	elif lang == 'ru':
		content = "Личная информация"
	else:
		content = "Personal information"
	return content


@register.simple_tag
def profile_address_information(lang=None):
	if lang == 'en':
		content = "Address information"
	elif lang == 'tr':
		content = "Adres bilgilerim"
	elif lang == 'bs':
		content = "Informacije o adresi"
	elif lang == 'ru':
		content = "Адресная информация"
	else:
		content = "Address information"
	return content


@register.simple_tag
def profile_contact_information(lang=None):
	if lang == 'en':
		content = "Contact information"
	elif lang == 'tr':
		content = "İletişim bilgilerim"
	elif lang == 'bs':
		content = "Kontakt informacije"
	elif lang == 'ru':
		content = "Информация для связи"
	else:
		content = "Contact information"
	return content


@register.simple_tag
def profile_date_of_birth(lang=None):
	if lang == 'en':
		content = "Date of birth"
	elif lang == 'tr':
		content = "Doğum tarihi"
	elif lang == 'bs':
		content = "Datum rođenja"
	elif lang == 'ru':
		content = "Дата рождения"
	else:
		content = "Date of birth"
	return content


@register.simple_tag
def profile_country(lang=None):
	if lang == 'en':
		content = "Country"
	elif lang == 'tr':
		content = "Ülke"
	elif lang == 'bs':
		content = "Država"
	elif lang == 'ru':
		content = "Страна"
	else:
		content = "Country"
	return content


@register.simple_tag
def profile_province(lang=None):
	if lang == 'en':
		content = "Province"
	elif lang == 'tr':
		content = "İl"
	elif lang == 'bs':
		content = "Regija"
	elif lang == 'ru':
		content = "Регион"
	else:
		content = "Province"
	return content


@register.simple_tag
def profile_city(lang=None):
	if lang == 'en':
		content = "City"
	elif lang == 'tr':
		content = "Şehir"
	elif lang == 'bs':
		content = "Grad"
	elif lang == 'ru':
		content = "Город"
	else:
		content = "City"
	return content


@register.simple_tag
def profile_address(lang=None):
	if lang == 'en':
		content = "Address"
	elif lang == 'tr':
		content = "Adres"
	elif lang == 'bs':
		content = "Adresa"
	elif lang == 'ru':
		content = "Адрес"
	else:
		content = "Address"
	return content


@register.simple_tag
def profile_mobile(lang=None):
	if lang == 'en':
		content = "Mobile phone number"
	elif lang == 'tr':
		content = "Cep tel. numarası"
	elif lang == 'bs':
		content = "Mobilni telefon"
	elif lang == 'ru':
		content = "Номер мобильного"
	else:
		content = "Mobile phone number"
	return content


@register.simple_tag
def profile_phone(lang=None):
	if lang == 'en':
		content = "Telephone"
	elif lang == 'tr':
		content = "Sabit tel. numarası"
	elif lang == 'bs':
		content = "Telefon"
	elif lang == 'ru':
		content = "Стационарный номер"
	else:
		content = "Telephone"
	return content


@register.simple_tag
def profile_phone_additional(lang=None):
	if lang == 'en':
		content = "Additional number"
	elif lang == 'tr':
		content = "Dahili numarası"
	elif lang == 'bs':
		content = "Dodatni telefon"
	elif lang == 'ru':
		content = "добавочный номер"
	else:
		content = "Additional number"
	return content


@register.simple_tag
def profile_first_name(lang=None):
	if lang == 'en':
		content = "First name"
	elif lang == 'tr':
		content = "İsim"
	elif lang == 'bs':
		content = "Ime"
	elif lang == 'ru':
		content = "Имя"
	else:
		content = "First name"
	return content


@register.simple_tag
def profile_last_name(lang=None):
	if lang == 'en':
		content = "Last name"
	elif lang == 'tr':
		content = "Soyisim"
	elif lang == 'bs':
		content = "Prezime"
	elif lang == 'ru':
		content = "Фамилия"
	else:
		content = "Last name"
	return content


@register.simple_tag
def profile_edit_profile(lang=None):
	if lang == 'en':
		content = "Edit profile"
	elif lang == 'tr':
		content = "Profilimi düzenle"
	elif lang == 'bs':
		content = "Uredi profil"
	elif lang == 'ru':
		content = "Изменить профиль"
	else:
		content = "Edit profile"
	return content


@register.simple_tag
def profile_save_profile(lang=None):
	if lang == 'en':
		content = "Save profile"
	elif lang == 'tr':
		content = "Profilimi kaydet"
	elif lang == 'bs':
		content = "Spremi profil"
	elif lang == 'ru':
		content = "Сохранить профиль"
	else:
		content = "Save profile"
	return content


@register.simple_tag
def general_cancel(lang=None):
	if lang == 'en':
		content = "Cancel"
	elif lang == 'tr':
		content = "İptal"
	elif lang == 'bs':
		content = "Otkaži"
	elif lang == 'ru':
		content = "Отмена"
	else:
		content = "Cancel"
	return content


@register.simple_tag
def general_my_company(lang=None):
	if lang == 'en':
		content = "My company"
	elif lang == 'tr':
		content = "Şirketim"
	elif lang == 'bs':
		content = "Moja firma"
	elif lang == 'ru':
		content = "Моя компания"
	else:
		content = "My company"
	return content


@register.simple_tag
def general_company_branches(lang=None):
	if lang == 'en':
		content = "Company branches"
	elif lang == 'tr':
		content = "Şirket şubeleri"
	elif lang == 'bs':
		content = "Filijale kompanije"
	elif lang == 'ru':
		content = "Филиалы компании"
	else:
		content = "Company branches"
	return content


@register.simple_tag
def general_application_users(lang=None):
	if lang == 'en':
		content = "Application Users"
	elif lang == 'tr':
		content = "Uygulama kullanıcıları"
	elif lang == 'bs':
		content = "Korisnici aplikacije"
	elif lang == 'ru':
		content = "Пользователи приложения"
	else:
		content = "Application Users"
	return content


@register.simple_tag
def general_user_groups(lang=None):
	if lang == 'en':
		content = "User groups"
	elif lang == 'tr':
		content = "Kullanıcı grupları"
	elif lang == 'bs':
		content = "Korisničke grupe"
	elif lang == 'ru':
		content = "Группы пользователей"
	else:
		content = "User groups"
	return content


@register.simple_tag
def general_company_manager(lang=None):
	if lang == 'en':
		content = "Company manager"
	elif lang == 'tr':
		content = "Şirket yönetimi"
	elif lang == 'bs':
		content = "Menadžer kompanije"
	elif lang == 'ru':
		content = "Управление компанийей"
	else:
		content = "Company manager"
	return content


@register.simple_tag
def general_all_rights_reserved(lang=None):
	if lang == 'en':
		content = "All rights reserved"
	elif lang == 'tr':
		content = "Tüm hakları saklıdır"
	elif lang == 'bs':
		content = "Sva prava zadržana"
	elif lang == 'ru':
		content = "Все права защищены"
	else:
		content = "All rights reserved"
	return content


@register.simple_tag
def general_docs(lang=None):
	if lang == 'en':
		content = "Docs"
	elif lang == 'tr':
		content = "Dokumantasyon"
	elif lang == 'bs':
		content = "Dokumentacija"
	elif lang == 'ru':
		content = "Документация"
	else:
		content = "Docs"
	return content


@register.simple_tag
def general_all_products(lang=None):
	if lang == 'en':
		content = "All products"
	elif lang == 'tr':
		content = "Tüm ürünlerimiz"
	elif lang == 'bs':
		content = "Svi naši proizvodi"
	elif lang == 'ru':
		content = "Все продукты"
	else:
		content = "All products"
	return content


@register.simple_tag
def general_manual(lang=None):
	if lang == 'en':
		content = "Manual"
	elif lang == 'tr':
		content = "Kılavuz"
	elif lang == 'bs':
		content = "Priručnik"
	elif lang == 'ru':
		content = "Инструкция"
	else:
		content = "Manual"
	return content


@register.simple_tag
def general_profile_information(lang=None):
	if lang == 'en':
		content = "Profile information"
	elif lang == 'tr':
		content = "Profil bilgileri"
	elif lang == 'bs':
		content = "Informacije o profilu"
	elif lang == 'ru':
		content = "Данные профиля"
	else:
		content = "Profile information"
	return content


@register.simple_tag
def general_change_password(lang=None):
	if lang == 'en':
		content = "Change password"
	elif lang == 'tr':
		content = "Şifre değiştir"
	elif lang == 'bs':
		content = "Promijeni lozinku"
	elif lang == 'ru':
		content = "Изменить пароль"
	else:
		content = "Change password"
	return content


@register.simple_tag
def general_profile_preferences(lang=None):
	if lang == 'en':
		content = "Profile preferences"
	elif lang == 'tr':
		content = "Profil ayarları"
	elif lang == 'bs':
		content = "Postavke profila"
	elif lang == 'ru':
		content = "Настройки профиля"
	else:
		content = "Profile preferences"
	return content


@register.simple_tag
def general_extra_profile_security(lang=None):
	if lang == 'en':
		content = "Extra profile security"
	elif lang == 'tr':
		content = "Ekstra profil güvenliği"
	elif lang == 'bs':
		content = "Dodatna sigurnost profila"
	elif lang == 'ru':
		content = "Доп. защита профиля"
	else:
		content = "Extra profile security"
	return content


@register.simple_tag
def general_client_manager(lang=None):
	if lang == 'en':
		content = "Client manager"
	elif lang == 'tr':
		content = "Müşteri yönetimi"
	elif lang == 'bs':
		content = "Mušterijin upravitelj"
	elif lang == 'ru':
		content = "Управление клиентами"
	else:
		content = "Client manager"
	return content


@register.simple_tag
def general_client_companies(lang=None):
	if lang == 'en':
		content = "Client Companies"
	elif lang == 'tr':
		content = "Müşteri şirketleri"
	elif lang == 'bs':
		content = "Kompanije mušterije"
	elif lang == 'ru':
		content = "Компании клиентов"
	else:
		content = "Client Companies"
	return content


@register.simple_tag
def general_client_branches(lang=None):
	if lang == 'en':
		content = "Client branches"
	elif lang == 'tr':
		content = "Müşteri şubeleri"
	elif lang == 'bs':
		content = "Filijale mušterije"
	elif lang == 'ru':
		content = "Филиалы компаний клиентов"
	else:
		content = "Client branches"
	return content


@register.simple_tag
def general_client_staff(lang=None):
	if lang == 'en':
		content = "Client staff"
	elif lang == 'tr':
		content = "Müsteri personeli"
	elif lang == 'bs':
		content = "Osoblje mušterije"
	elif lang == 'ru':
		content = "Персонал компаний клиентов"
	else:
		content = "Client staff"
	return content


@register.simple_tag
def general_vehicle_agents(lang=None):
	if lang == 'en':
		content = "Vehicle Agents"
	elif lang == 'tr':
		content = "Taşıma ajentaları"
	elif lang == 'bs':
		content = "Zastupnik vozila"
	elif lang == 'ru':
		content = "Агентства грузоперевозок"
	else:
		content = "Vehicle Agents"
	return content


@register.simple_tag
def general_crm_and_trading(lang=None):
	if lang == 'en':
		content = "CRM and Trading"
	elif lang == 'tr':
		content = "CRM ve Pazarlama"
	elif lang == 'bs':
		content = "CRM i prodaja"
	elif lang == 'ru':
		content = "CRM и Трейдинг"
	else:
		content = "CRM and Trading"
	return content


@register.simple_tag
def general_trading_management(lang=None):
	if lang == 'en':
		content = "Trading management"
	elif lang == 'tr':
		content = "Pazarlama yönetimi"
	elif lang == 'bs':
		content = "Upravljanje prodaje"
	elif lang == 'ru':
		content = "Управление торговлей"
	else:
		content = "Trading management"
	return content


@register.simple_tag
def general_offers(lang=None):
	if lang == 'en':
		content = "Offers"
	elif lang == 'tr':
		content = "Teklifler"
	elif lang == 'bs':
		content = "Ponude"
	elif lang == 'ru':
		content = "Предложения"
	else:
		content = "Offers"
	return content


@register.simple_tag
def general_appointments(lang=None):
	if lang == 'en':
		content = "Appointments"
	elif lang == 'tr':
		content = "Görüşmeler"
	elif lang == 'bs':
		content = "Dogovori"
	elif lang == 'ru':
		content = "Собрания и встречи"
	else:
		content = "Appointments"
	return content


@register.simple_tag
def general_reports(lang=None):
	if lang == 'en':
		content = "Reports"
	elif lang == 'tr':
		content = "Raporlar"
	elif lang == 'bs':
		content = "Izvještaji"
	elif lang == 'ru':
		content = "Отчеты"
	else:
		content = "Reports"
	return content


@register.simple_tag
def general_vehicle_manager(lang=None):
	if lang == 'en':
		content = "Vehicle Manager"
	elif lang == 'tr':
		content = "Taşımacılık yönetimi"
	elif lang == 'bs':
		content = "Upravitelj vozilima"
	elif lang == 'ru':
		content = "Упр. транспортными средствами"
	else:
		content = "Vehicle Manager"
	return content


@register.simple_tag
def general_land_vehicle(lang=None):
	if lang == 'en':
		content = "Land Vehicle"
	elif lang == 'tr':
		content = "Kara taşımacılığı"
	elif lang == 'bs':
		content = "Kopnena vozila"
	elif lang == 'ru':
		content = "Сухопутный транспорт"
	else:
		content = "Land Vehicle"
	return content


@register.simple_tag
def general_air_craft(lang=None):
	if lang == 'en':
		content = "Air Craft"
	elif lang == 'tr':
		content = "Hawa taşımacılığı"
	elif lang == 'bs':
		content = "Avioni"
	elif lang == 'ru':
		content = "Воздушные суда"
	else:
		content = "Air Craft"
	return content


@register.simple_tag
def general_sea_craft(lang=None):
	if lang == 'en':
		content = "Sea Craft"
	elif lang == 'tr':
		content = "Deniz taşımacılığı"
	elif lang == 'bs':
		content = "Brodovi"
	elif lang == 'ru':
		content = "Морские суда"
	else:
		content = "Sea Craft"
	return content


@register.simple_tag
def general_additionals_services(lang=None):
	if lang == 'en':
		content = "Additional services"
	elif lang == 'tr':
		content = "Ek hizmetler"
	elif lang == 'bs':
		content = "Dodatne usluge"
	elif lang == 'ru':
		content = "Дополнительные сервисы"
	else:
		content = "Additional services"
	return content


@register.simple_tag
def general_vehicle_drivers(lang=None):
	if lang == 'en':
		content = "Vehicle drivers"
	elif lang == 'tr':
		content = "Taşıma şoförleri"
	elif lang == 'bs':
		content = "Vozači"
	elif lang == 'ru':
		content = "Водители"
	else:
		content = "Vehicle drivers"
	return content


@register.simple_tag
def general_cargo_management(lang=None):
	if lang == 'en':
		content = "Cargo Management"
	elif lang == 'tr':
		content = "Kargo yönetimi"
	elif lang == 'bs':
		content = "Upravljanje dostave"
	elif lang == 'ru':
		content = "Управление грузоперевозками"
	else:
		content = "Cargo Management"
	return content


@register.simple_tag
def general_task_manager(lang=None):
	if lang == 'en':
		content = "Task Manager"
	elif lang == 'tr':
		content = "Görev yöneticisi"
	elif lang == 'bs':
		content = "Upravitelj zadataka"
	elif lang == 'ru':
		content = "Управление задачами"
	else:
		content = "Task Manager"
	return content


@register.simple_tag
def general_messages_and_chats(lang=None):
	if lang == 'en':
		content = "Messages and Chats"
	elif lang == 'tr':
		content = "Mesajlar ve sohbetler"
	elif lang == 'bs':
		content = "Poruke i chat"
	elif lang == 'ru':
		content = "Сообщения и чаты"
	else:
		content = "Messages and Chats"
	return content


@register.simple_tag
def general_faq(lang=None):
	if lang == 'en':
		content = "F.A.Q"
	elif lang == 'tr':
		content = "S.S.S."
	elif lang == 'bs':
		content = "F.A.Q"
	elif lang == 'ru':
		content = "Ч.З.В."
	else:
		content = "F.A.Q"
	return content


@register.simple_tag
def general_tutorials(lang=None):
	if lang == 'en':
		content = "Tutorials"
	elif lang == 'tr':
		content = "Öğretici içerikler"
	elif lang == 'bs':
		content = "Tutorijali"
	elif lang == 'ru':
		content = "Обучающие уроки"
	else:
		content = "Tutorials"
	return content


@register.simple_tag
def general_commercial_offers(lang=None):
	if lang == 'en':
		content = "Commercial offers"
	elif lang == 'tr':
		content = "İş teklifleri"
	elif lang == 'bs':
		content = "Poslovne ponude"
	elif lang == 'ru':
		content = "Коммерческие предложения"
	else:
		content = "Commercial offers"
	return content


@register.simple_tag
def general_to_filter(lang=None):
	if lang == 'en':
		content = "Filter"
	elif lang == 'tr':
		content = "Filtrele"
	elif lang == 'bs':
		content = "Filter"
	elif lang == 'ru':
		content = "Фильтровать"
	else:
		content = "Filter"
	return content


@register.simple_tag
def general_commercial_offer(lang=None):
	if lang == 'en':
		content = "Commercial offer"
	elif lang == 'tr':
		content = "İş teklifi"
	elif lang == 'bs':
		content = "Poslovna ponuda"
	elif lang == 'ru':
		content = "Коммерческое предложение"
	else:
		content = "Commercial offer"
	return content


@register.simple_tag
def general_filter(lang=None):
	if lang == 'en':
		content = "Filter"
	elif lang == 'tr':
		content = "Filtrele"
	elif lang == 'bs':
		content = "Filter"
	elif lang == 'ru':
		content = "Фильтр"
	else:
		content = "Filter"
	return content


@register.simple_tag
def general_order_by(lang=None):
	if lang == 'en':
		content = "Order by"
	elif lang == 'tr':
		content = "Sırala"
	elif lang == 'bs':
		content = "Poredaj"
	elif lang == 'ru':
		content = "Сортировать по"
	else:
		content = "Order by"
	return content


@register.simple_tag
def general_show_by(lang=None):
	if lang == 'en':
		content = "Show by"
	elif lang == 'tr':
		content = "Göster"
	elif lang == 'bs':
		content = "Prikaži po"
	elif lang == 'ru':
		content = "Показывать по"
	else:
		content = "Show by"
	return content


@register.simple_tag
def general_first(lang=None):
	if lang == 'en':
		content = "First"
	elif lang == 'tr':
		content = "Grad"
	elif lang == 'bs':
		content = "Prvi"
	elif lang == 'ru':
		content = "Первый"
	else:
		content = "First"
	return content


@register.simple_tag
def general_last(lang=None):
	if lang == 'en':
		content = "Last"
	elif lang == 'tr':
		content = "Son"
	elif lang == 'bs':
		content = "Zadnji"
	elif lang == 'ru':
		content = "Последний"
	else:
		content = "Last"
	return content


@register.simple_tag
def general_next(lang=None):
	if lang == 'en':
		content = "Next"
	elif lang == 'tr':
		content = "Sonraki"
	elif lang == 'bs':
		content = "Sljedeći"
	elif lang == 'ru':
		content = "Следующий"
	else:
		content = "Next"
	return content


@register.simple_tag
def general_previous(lang=None):
	if lang == 'en':
		content = "Previous"
	elif lang == 'tr':
		content = "Önceki"
	elif lang == 'bs':
		content = "Prethodni"
	elif lang == 'ru':
		content = "Предидущий"
	else:
		content = "Previous"
	return content


@register.simple_tag
def general_save(lang=None):
	if lang == 'en':
		content = "Save"
	elif lang == 'tr':
		content = "Kaydet"
	elif lang == 'bs':
		content = "Spremi"
	elif lang == 'ru':
		content = "Сохранить"
	else:
		content = "Save"
	return content


@register.simple_tag
def general_edit(lang=None):
	if lang == 'en':
		content = "Edit"
	elif lang == 'tr':
		content = "Değiştir"
	elif lang == 'bs':
		content = "Uredi"
	elif lang == 'ru':
		content = "Изменить"
	else:
		content = "Edit"
	return content


@register.simple_tag
def general_yes(lang=None):
	if lang == 'en':
		content = "Yes"
	elif lang == 'tr':
		content = "Evet"
	elif lang == 'bs':
		content = "Da"
	elif lang == 'ru':
		content = "Да"
	else:
		content = "Yes"
	return content


@register.simple_tag
def general_no(lang=None):
	if lang == 'en':
		content = "No"
	elif lang == 'tr':
		content = "Hayır"
	elif lang == 'bs':
		content = "Ne"
	elif lang == 'ru':
		content = "Нет"
	else:
		content = "No"
	return content


@register.simple_tag
def general_branch(lang=None):
	if lang == 'en':
		content = "Branch"
	elif lang == 'tr':
		content = "Şube"
	elif lang == 'bs':
		content = "Filijala"
	elif lang == 'ru':
		content = "Филиал"
	else:
		content = "Branch"
	return content


@register.simple_tag
def general_branch_name(lang=None):
	if lang == 'en':
		content = "Branch name"
	elif lang == 'tr':
		content = "Şube adı"
	elif lang == 'bs':
		content = "Ime filijale"
	elif lang == 'ru':
		content = "Название филиала"
	else:
		content = "Branch name"
	return content


@register.simple_tag
def general_company_name(lang=None):
	if lang == 'en':
		content = "Company name"
	elif lang == 'tr':
		content = "Şirket ünvanı"
	elif lang == 'bs':
		content = "Ime kompanije"
	elif lang == 'ru':
		content = "Название компание"
	else:
		content = "Company name"
	return content


@register.simple_tag
def general_save_changes(lang=None):
	if lang == 'en':
		content = "Save changes"
	elif lang == 'tr':
		content = "Değişiklikleri kaydet"
	elif lang == 'bs':
		content = "Spremi promjene"
	elif lang == 'ru':
		content = "Сохранить изменения"
	else:
		content = "Save changes"
	return content


@register.simple_tag
def general_main_office(lang=None):
	if lang == 'en':
		content = "Main office"
	elif lang == 'tr':
		content = "Merkez ofis"
	elif lang == 'bs':
		content = "Glavni ured"
	elif lang == 'ru':
		content = "Главный офис"
	else:
		content = "Main office"
	return content


@register.simple_tag
def general_fax_number(lang=None):
	if lang == 'en':
		content = "Fax number"
	elif lang == 'tr':
		content = "Faks numarası"
	elif lang == 'bs':
		content = "Faks"
	elif lang == 'ru':
		content = "Номер факса"
	else:
		content = "Fax number"
	return content


@register.simple_tag
def general_logo(lang=None):
	if lang == 'en':
		content = "Logo"
	elif lang == 'tr':
		content = "Logo"
	elif lang == 'bs':
		content = "Logo"
	elif lang == 'ru':
		content = "Логотип"
	else:
		content = "Logo"
	return content


@register.simple_tag
def general_avatar(lang=None):
	if lang == 'en':
		content = "Avatar"
	elif lang == 'tr':
		content = "Avatar"
	elif lang == 'bs':
		content = "Avatar"
	elif lang == 'ru':
		content = "Аватар"
	else:
		content = "Avatar"
	return content


@register.simple_tag
def general_profile_image(lang=None):
	if lang == 'en':
		content = "Profile image"
	elif lang == 'tr':
		content = "Profil resmi"
	elif lang == 'bs':
		content = "Slika profila"
	elif lang == 'ru':
		content = "Фотография профиля"
	else:
		content = "Profile image"
	return content


@register.simple_tag
def general_crop(lang=None):
	if lang == 'en':
		content = "Crop"
	elif lang == 'tr':
		content = "Kes"
	elif lang == 'bs':
		content = "Isijeci"
	elif lang == 'ru':
		content = "Обрезать"
	else:
		content = "Crop"
	return content


@register.simple_tag
def general_support_information(lang=None):
	if lang == 'en':
		content = "Support information"
	elif lang == 'tr':
		content = "Destek bilgileri"
	elif lang == 'bs':
		content = "Informacija o podršci"
	elif lang == 'ru':
		content = "Справочная информация"
	else:
		content = "Support information"
	return content


@register.simple_tag
def company_staff_manager(lang=None):
	if lang == 'en':
		content = "Staff Manager"
	elif lang == 'tr':
		content = "Personel yönetimi"
	elif lang == 'bs':
		content = "Upravitelj zaposlenicima"
	elif lang == 'ru':
		content = "Управление персоналом"
	else:
		content = "Staff Manager"
	return content


@register.simple_tag
def company_staff_list(lang=None):
	if lang == 'en':
		content = "Staff List"
	elif lang == 'tr':
		content = "Personel listesi"
	elif lang == 'bs':
		content = "Lista zaposlenika"
	elif lang == 'ru':
		content = "Список персонала"
	else:
		content = "Staff List"
	return content


@register.simple_tag
def company_employee(lang=None):
	if lang == 'en':
		content = "Employee"
	elif lang == 'tr':
		content = "Eleman"
	elif lang == 'bs':
		content = "Zaposlenik"
	elif lang == 'ru':
		content = "Работник"
	else:
		content = "Employee"
	return content


@register.simple_tag
def company_company_positions(lang=None):
	if lang == 'en':
		content = "Company positions"
	elif lang == 'tr':
		content = "İş pozisyonları"
	elif lang == 'bs':
		content = "Pozicije u kompanije"
	elif lang == 'ru':
		content = "Должности в компании"
	else:
		content = "Company positions"
	return content


@register.simple_tag
def company_company_position(lang=None):
	if lang == 'en':
		content = "Company position"
	elif lang == 'tr':
		content = "İş pozisyonu"
	elif lang == 'bs':
		content = "Pozicija u kompaniji"
	elif lang == 'ru':
		content = "Должность в компании"
	else:
		content = "Company position"
	return content


@register.simple_tag
def company_position(lang=None):
	if lang == 'en':
		content = "Position"
	elif lang == 'tr':
		content = "Pozisyon"
	elif lang == 'bs':
		content = "Pozicija"
	elif lang == 'ru':
		content = "Должность"
	else:
		content = "Position"
	return content


@register.simple_tag
def company_positions(lang=None):
	if lang == 'en':
		content = "Positions"
	elif lang == 'tr':
		content = "Pozisyonlar"
	elif lang == 'bs':
		content = "Pozicije"
	elif lang == 'ru':
		content = "Должности"
	else:
		content = "Positions"
	return content


@register.simple_tag
def company_employees(lang=None):
	if lang == 'en':
		content = "Employees"
	elif lang == 'tr':
		content = "Elemanlar"
	elif lang == 'bs':
		content = "Zaposlenici"
	elif lang == 'ru':
		content = "Работники"
	else:
		content = "Employees"
	return content


@register.simple_tag
def general_add(lang=None):
	if lang == 'en':
		content = "Add"
	elif lang == 'tr':
		content = "Ekle"
	elif lang == 'bs':
		content = "Dodati"
	elif lang == 'ru':
		content = "Добавить"
	else:
		content = "Add"
	return content


@register.simple_tag
def general_delete(lang=None):
	if lang == 'en':
		content = "Delete"
	elif lang == 'tr':
		content = "Sil"
	elif lang == 'bs':
		content = "Izbrisati"
	elif lang == 'ru':
		content = "Удалить"
	else:
		content = "Delete"
	return content


@register.simple_tag
def general_enable(lang=None):
	if lang == 'en':
		content = "Enable"
	elif lang == 'tr':
		content = "Etkinleştir"
	elif lang == 'bs':
		content = "Omogući"
	elif lang == 'ru':
		content = "Включить"
	else:
		content = "Enable"
	return content


@register.simple_tag
def general_disable(lang=None):
	if lang == 'en':
		content = "Disable"
	elif lang == 'tr':
		content = "Devre dışı"
	elif lang == 'bs':
		content = "Onemogući"
	elif lang == 'ru':
		content = "Отключить"
	else:
		content = "Disable"
	return content


@register.simple_tag
def general_activate(lang=None):
	if lang == 'en':
		content = "Activate"
	elif lang == 'tr':
		content = "Aktif et"
	elif lang == 'bs':
		content = "Aktiviraj"
	elif lang == 'ru':
		content = "Активировать"
	else:
		content = "Activate"
	return content


@register.simple_tag
def general_deactivate(lang=None):
	if lang == 'en':
		content = "Deactivate"
	elif lang == 'tr':
		content = "Devre dışı bırak"
	elif lang == 'bs':
		content = "Deaktiviraj"
	elif lang == 'ru':
		content = "Деактивировать"
	else:
		content = "Deactivate"
	return content


@register.simple_tag
def general_block(lang=None):
	if lang == 'en':
		content = "Block"
	elif lang == 'tr':
		content = "Bloke et"
	elif lang == 'bs':
		content = "Blokiraj"
	elif lang == 'ru':
		content = "Заблокировать"
	else:
		content = "Block"
	return content


@register.simple_tag
def general_unblock(lang=None):
	if lang == 'en':
		content = "Unblock"
	elif lang == 'tr':
		content = "Blokeyi kaldır"
	elif lang == 'bs':
		content = "Deblokiraj"
	elif lang == 'ru':
		content = "Разблокировать"
	else:
		content = "Unblock"
	return content


@register.simple_tag
def admin_user_access_group(lang=None):
	if lang == 'en':
		content = "User access group"
	elif lang == 'tr':
		content = "Kullanıcı yetki grubu"
	elif lang == 'bs':
		content = "Korisnička pristupna grupa"
	elif lang == 'ru':
		content = "Группа разрешений"
	else:
		content = "User access group"
	return content


@register.simple_tag
def admin_user_access_groups(lang=None):
	if lang == 'en':
		content = "User access groups"
	elif lang == 'tr':
		content = "Kullanıcı yetki grupları"
	elif lang == 'bs':
		content = "Korisničke pristupne grupe"
	elif lang == 'ru':
		content = "Группы разрешений"
	else:
		content = "User access groups"
	return content


@register.simple_tag
def admin_user_access_permission(lang=None):
	if lang == 'en':
		content = "User access permission"
	elif lang == 'tr':
		content = "Kullanıcı yetki"
	elif lang == 'bs':
		content = "Korisnička pristupna dozvola"
	elif lang == 'ru':
		content = "Разрешение"
	else:
		content = "User access permission"
	return content


@register.simple_tag
def admin_application_user(lang=None):
	if lang == 'en':
		content = "Application user"
	elif lang == 'tr':
		content = "Program kullanıcı"
	elif lang == 'bs':
		content = "Korisnik aplikacije"
	elif lang == 'ru':
		content = "Системный пользователь"
	else:
		content = "Application user"
	return content


@register.simple_tag
def admin_application_users(lang=None):
	if lang == 'en':
		content = "Application users"
	elif lang == 'tr':
		content = "Program kullanıcıları"
	elif lang == 'bs':
		content = "Korisnici aplikacije"
	elif lang == 'ru':
		content = "Системные пользователи"
	else:
		content = "Application users"
	return content


@register.simple_tag
def admin_application_admin(lang=None):
	if lang == 'en':
		content = "Application admin"
	elif lang == 'tr':
		content = "Program admini"
	elif lang == 'bs':
		content = "Admin aplikacije"
	elif lang == 'ru':
		content = "Системный администратор"
	else:
		content = "Application admin"
	return content


@register.simple_tag
def admin_application_admins(lang=None):
	if lang == 'en':
		content = "Application admins"
	elif lang == 'tr':
		content = "Program adminleri"
	elif lang == 'bs':
		content = "Admini aplikacije"
	elif lang == 'ru':
		content = "Системные администраторы"
	else:
		content = "Application admins"
	return content


@register.simple_tag
def general_title(lang=None):
	if lang == 'en':
		content = "Title"
	elif lang == 'tr':
		content = "Adı"
	elif lang == 'bs':
		content = "Naslov"
	elif lang == 'ru':
		content = "Наименование"
	else:
		content = "Title"
	return content


@register.simple_tag
def general_id(lang=None):
	if lang == 'en':
		content = "ID"
	elif lang == 'tr':
		content = "ID"
	elif lang == 'bs':
		content = "ID"
	elif lang == 'ru':
		content = "ID"
	else:
		content = "ID"
	return content


@register.simple_tag
def general_alias(lang=None):
	if lang == 'en':
		content = "Alias"
	elif lang == 'tr':
		content = "Sistem adı"
	elif lang == 'bs':
		content = "Pseudonim"
	elif lang == 'ru':
		content = "Системный псевдоним"
	else:
		content = "Alias"
	return content


@register.simple_tag
def general_uuid(lang=None):
	if lang == 'en':
		content = "UUID"
	elif lang == 'tr':
		content = "UUID"
	elif lang == 'bs':
		content = "UUID"
	elif lang == 'ru':
		content = "UUID"
	else:
		content = "UUID"
	return content


@register.simple_tag
def general_action(lang=None):
	if lang == 'en':
		content = "Action"
	elif lang == 'tr':
		content = "Eylem"
	elif lang == 'bs':
		content = "Akcija"
	elif lang == 'ru':
		content = "Действие"
	else:
		content = "Action"
	return content


@register.simple_tag
def general_remove(lang=None):
	if lang == 'en':
		content = "Remove"
	elif lang == 'tr':
		content = "Kaldır"
	elif lang == 'bs':
		content = "Ukloni"
	elif lang == 'ru':
		content = "Убрать"
	else:
		content = "Remove"
	return content


@register.simple_tag
def general_enter_title(lang=None):
	if lang == 'en':
		content = "Enter title"
	elif lang == 'tr':
		content = "Adını giriniz"
	elif lang == 'bs':
		content = "Unesi naslov"
	elif lang == 'ru':
		content = "Введите наименование"
	else:
		content = "Enter title"
	return content


@register.simple_tag
def general_enter_alias(lang=None):
	if lang == 'en':
		content = "Enter alias"
	elif lang == 'tr':
		content = "Sistem adını giriniz"
	elif lang == 'bs':
		content = "Unesi pseudonim"
	else:
		content = "Enter alias"
	return content


@register.simple_tag
def admin_add_user_access_group(lang=None):
	if lang == 'en':
		content = "Add user access group"
	elif lang == 'tr':
		content = "Kullanıcı yetki grubu ekle"
	elif lang == 'bs':
		content = "Dodaj novu grupu korisnika za pristup"
	elif lang == 'ru':
		content = "Добавить группу разрешений"
	else:
		content = "Add user access group"
	return content


@register.simple_tag
def admin_edit_user_access_group(lang=None):
	if lang == 'en':
		content = "Edit user access group"
	elif lang == 'tr':
		content = "Kullanıcı yetki grubu değiştir"
	elif lang == 'bs':
		content = "Uredi grupu korisnika za pristup"
	elif lang == 'ru':
		content = "Изменить группу разрешений"
	else:
		content = "Edit user access group"
	return content


@register.simple_tag
def admin_delete_user_access_group(lang=None):
	if lang == 'en':
		content = "Delete user  access group"
	elif lang == 'tr':
		content = "Kullanıcı yetki grubu sil"
	elif lang == 'bs':
		content = "Izbriši grupu korisnika za pristup"
	elif lang == 'ru':
		content = "Удалить группу разрешений"
	else:
		content = "Delete user  access group"
	return content


@register.simple_tag
def admin_add_user_access_permission(lang=None):
	if lang == 'en':
		content = "Add user access permission"
	elif lang == 'tr':
		content = "Kullanıcı yetki ekle"
	elif lang == 'bs':
		content = "Dodaj korisničku pristupnu dozvolu"
	elif lang == 'ru':
		content = "Добавить разрешение"
	else:
		content = "Add user access permission"
	return content


@register.simple_tag
def admin_edit_user_access_permission(lang=None):
	if lang == 'en':
		content = "Edit user access permission"
	elif lang == 'tr':
		content = "Kullanıcı yetki değiştir"
	elif lang == 'bs':
		content = "Uredi korisničku pristupnu dozvolu"
	elif lang == 'ru':
		content = "Изменить разрешение"
	else:
		content = "Edit user access permission"
	return content


@register.simple_tag
def admin_delete_user_access_permission(lang=None):
	if lang == 'en':
		content = "Delete user access permission"
	elif lang == 'tr':
		content = "Kullanıcı yetki sil"
	elif lang == 'bs':
		content = "Izbriši korisničku pristupnu dozvolu"
	elif lang == 'ru':
		content = "Удалить разрешение"
	else:
		content = "Delete user access permission"
	return content


@register.simple_tag
def admin_add_user_access_permissions(lang=None):
	if lang == 'en':
		content = "Add user access permissions"
	elif lang == 'tr':
		content = "Kullanıcı yetkileri ekle"
	elif lang == 'bs':
		content = "Dodaj korisničke pristupne dozvole"
	elif lang == 'ru':
		content = "Добавить разрешения"
	else:
		content = "Add user access permissions"
	return content


@register.simple_tag
def general_actions(lang=None):
	if lang == 'en':
		content = "Actions"
	elif lang == 'tr':
		content = "Eylemler"
	elif lang == 'bs':
		content = "Akcije"
	elif lang == 'ru':
		content = "Действия"
	else:
		content = "Actions"
	return content


@register.simple_tag
def access_group_access_templates(lang=None):
	if lang == 'en':
		content = "Access Templates"
	elif lang == 'tr':
		content = "Erişim şablonları"
	elif lang == 'bs':
		content = "Pristupni predlošci"
	elif lang == 'ru':
		content = "Шаблоны прав"
	else:
		content = "Access Templates"
	return content


@register.simple_tag
def datawall_collectors(lang=None):
	if lang == 'en':
		content = "Collectors"
	elif lang == 'tr':
		content = "Toplayıcılar"
	elif lang == 'bs':
		content = "Kolektor"
	else:
		content = "Collectors"
	return content


@register.simple_tag
def datawall_storages(lang=None):
	if lang == 'en':
		content = "Storages"
	elif lang == 'tr':
		content = "Depolama"
	elif lang == 'bs':
		content = "Skladišta"
	elif lang == 'ru':
		content = "Хранилища"
	else:
		content = "Storages"
	return content


@register.simple_tag
def datawall_stored_data(lang=None):
	if lang == 'en':
		content = "Stored Data"
	elif lang == 'tr':
		content = "Depolanan veriler"
	elif lang == 'bs':
		content = "Spremljeni podaci"
	elif lang == 'ru':
		content = "Сохраненные данные"
	else:
		content = "Stored Data"
	return content


@register.simple_tag
def datawall_notification_rules(lang=None):
	if lang == 'en':
		content = "Notification Rules"
	elif lang == 'tr':
		content = "Bildirim kuralları"
	elif lang == 'bs':
		content = "Pravila obavijesti"
	elif lang == 'ru':
		content = "Правила уведомлений"
	else:
		content = "Notification Rules"
	return content


@register.simple_tag
def datawall_configuration(lang=None):
	if lang == 'en':
		content = "Configuration"
	elif lang == 'tr':
		content = "Yapılandırma"
	elif lang == 'bs':
		content = "Postavke"
	elif lang == 'ru':
		content = "Настройки"
	else:
		content = "Configuration"
	return content


@register.simple_tag
def definition_users_manager(lang=None):
	if lang == 'en':
		content = "Users manager"
	elif lang == 'tr':
		content = "Kullanıcılar yöneticisi"
	elif lang == 'bs':
		content = "Menadžer korisnika"
	elif lang == 'ru':
		content = "Управление пользователями"
	else:
		content = "Users manager"
	return content


@register.simple_tag
def definition_location(lang=None):
	if lang == 'en':
		content = "Location"
	elif lang == 'tr':
		content = "Yer"
	elif lang == 'bs':
		content = "Lokacija"
	elif lang == 'ru':
		content = "Местоположение"
	else:
		content = "Location"
	return content


@register.simple_tag
def definition_locations(lang=None):
	if lang == 'en':
		content = "Locations"
	elif lang == 'tr':
		content = "Yerler"
	elif lang == 'bs':
		content = "Lokacije"
	elif lang == 'ru':
		content = "Местоположения"
	else:
		content = "Locations"
	return content


@register.simple_tag
def definition_country(lang=None):
	if lang == 'en':
		content = "Country"
	elif lang == 'tr':
		content = "Ülke"
	elif lang == 'bs':
		content = "Država"
	elif lang == 'ru':
		content = "Страна"
	else:
		content = "Country"
	return content


@register.simple_tag
def definition_countries(lang=None):
	if lang == 'en':
		content = "Countries"
	elif lang == 'tr':
		content = "Ülkeler"
	elif lang == 'bs':
		content = "Države"
	elif lang == 'ru':
		content = "Страны"
	else:
		content = "Countries"
	return content


@register.simple_tag
def definition_add_new_country(lang=None):
	if lang == 'en':
		content = "Add new country"
	elif lang == 'tr':
		content = "Yeni ülke ekle"
	elif lang == 'bs':
		content = "Dodaj novu državu"
	elif lang == 'ru':
		content = "Добавить новую страну"
	else:
		content = "Add new country"
	return content


@register.simple_tag
def definition_state(lang=None):
	if lang == 'en':
		content = "State"
	elif lang == 'tr':
		content = "İl"
	elif lang == 'bs':
		content = "Provincija"
	elif lang == 'ru':
		content = "Область"
	else:
		content = "State"
	return content


@register.simple_tag
def definition_states(lang=None):
	if lang == 'en':
		content = "States"
	elif lang == 'tr':
		content = "İller"
	elif lang == 'bs':
		content = "Provincije"
	elif lang == 'ru':
		content = "Области"
	else:
		content = "States"
	return content


@register.simple_tag
def definition_add_new_state(lang=None):
	if lang == 'en':
		content = "Add new state"
	elif lang == 'tr':
		content = "Yeni il ekle"
	elif lang == 'bs':
		content = "Dodaj novu provinciju"
	elif lang == 'ru':
		content = "Добавить новую область"
	else:
		content = "Add new state"
	return content


@register.simple_tag
def definition_city(lang=None):
	if lang == 'en':
		content = "City"
	elif lang == 'tr':
		content = "İlçe"
	elif lang == 'bs':
		content = "Grad"
	elif lang == 'ru':
		content = "Город"
	else:
		content = "City"
	return content


@register.simple_tag
def definition_cities(lang=None):
	if lang == 'en':
		content = "Cities"
	elif lang == 'tr':
		content = "İlçeler"
	elif lang == 'bs':
		content = "Gradovi"
	elif lang == 'ru':
		content = "Города"
	else:
		content = "Cities"
	return content


@register.simple_tag
def definition_add_new_city(lang=None):
	if lang == 'en':
		content = "Add new city"
	elif lang == 'tr':
		content = "Yeni şehir ekle"
	elif lang == 'bs':
		content = "Dodaj novi grad"
	elif lang == 'ru':
		content = "Добавить новый город"
	else:
		content = "Add new city"
	return content


@register.simple_tag
def definition_code(lang=None):
	if lang == 'en':
		content = "Code"
	elif lang == 'tr':
		content = "Kod"
	elif lang == 'bs':
		content = "Kod"
	elif lang == 'ru':
		content = "Код"
	else:
		content = "Code"
	return content


@register.simple_tag
def definition_status(lang=None):
	if lang == 'en':
		content = "Status"
	elif lang == 'tr':
		content = "Durum"
	elif lang == 'bs':
		content = "Status"
	elif lang == 'ru':
		content = "Статус"
	else:
		content = "Status"
	return content


@register.simple_tag
def definition_cargo_definitions(lang=None):
	if lang == 'en':
		content = "Cargo definitions"
	elif lang == 'tr':
		content = "Kargo tanımları"
	elif lang == 'bs':
		content = "Kargo"
	elif lang == 'ru':
		content = "Данные перевозок"
	else:
		content = "Cargo definitions"
	return content


@register.simple_tag
def definition_cargo_type(lang=None):
	if lang == 'en':
		content = "Cargo type"
	elif lang == 'tr':
		content = "Kargo tipi"
	elif lang == 'bs':
		content = "Tip karga"
	elif lang == 'ru':
		content = "Тип перевозки"
	else:
		content = "Cargo type"
	return content


@register.simple_tag
def definition_cargo_types(lang=None):
	if lang == 'en':
		content = "Cargo types"
	elif lang == 'tr':
		content = "Kargo tipleri"
	elif lang == 'bs':
		content = "Tipovi karga"
	elif lang == 'ru':
		content = "Типы перевозок"
	else:
		content = "Cargo types"
	return content


@register.simple_tag
def definition_add_new_cargo_type(lang=None):
	if lang == 'en':
		content = "Add new cargo type"
	elif lang == 'tr':
		content = "Yeni kargo tipi ekle"
	elif lang == 'bs':
		content = "Dodaj novi tip karga"
	elif lang == 'ru':
		content = "Добавить новый тип перевозки"
	else:
		content = "Add new cargo type"
	return content


@register.simple_tag
def definition_container_type(lang=None):
	if lang == 'en':
		content = "Container type"
	elif lang == 'tr':
		content = "Konteyner tip"
	elif lang == 'bs':
		content = "Tip kontejnera"
	elif lang == 'ru':
		content = "Тип контейнера"
	else:
		content = "Container type"
	return content


@register.simple_tag
def definition_container_types(lang=None):
	if lang == 'en':
		content = "Container types"
	elif lang == 'tr':
		content = "Konteyner tipleri"
	elif lang == 'bs':
		content = "Tipovi kontejnera"
	elif lang == 'ru':
		content = "Типы контейнеров"
	else:
		content = "Container types"
	return content


@register.simple_tag
def definition_add_new_container_type(lang=None):
	if lang == 'en':
		content = "Add new container type"
	elif lang == 'tr':
		content = "Yeni kargo tipi ekle"
	elif lang == 'bs':
		content = "Dodaj novi tip kontejnera"
	elif lang == 'ru':
		content = "Добавить новый тип контейнера"
	else:
		content = "Add new container type"
	return content


@register.simple_tag
def definition_incoterms_list(lang=None):
	if lang == 'en':
		content = "Incoterms list"
	elif lang == 'bs':
		content = "Lista incoterm-a"
	elif lang == 'ru':
		content = "Список INCOTERMS"
	else:
		content = "Incoterms list"
	return content


@register.simple_tag
def definition_incoterm(lang=None):
	if lang == 'en':
		content = "Incoterm"
	elif lang == 'bs':
		content = "Incoterm"
	elif lang == 'ru':
		content = "Incoterm"
	else:
		content = "Incoterm"
	return content


@register.simple_tag
def definition_add_new_incoterm(lang=None):
	if lang == 'en':
		content = "Add new incoterm"
	elif lang == 'bs':
		content = "Dodaj novi incoterm"
	elif lang == 'ru':
		content = "Добавить INCOTERM"
	else:
		content = "Add new incoterm"
	return content


@register.simple_tag
def definition_description(lang=None):
	if lang == 'en':
		content = "Description"
	elif lang == 'tr':
		content = "Açıklama"
	elif lang == 'bs':
		content = "Opis"
	elif lang == 'ru':
		content = "Описание"
	else:
		content = "Description"
	return content


@register.simple_tag
def profile_position(lang=None):
	if lang == 'en':
		content = "Position"
	elif lang == 'tr':
		content = "Pozisyon"
	elif lang == 'bs':
		content = "Pozicija"
	elif lang == 'ru':
		content = "Должность"
	else:
		content = "Position"
	return content


@register.simple_tag
def profile_positions(lang=None):
	if lang == 'en':
		content = "Positions"
	elif lang == 'tr':
		content = "Pozisyonlar"
	elif lang == 'bs':
		content = "Pozicije"
	elif lang == 'ru':
		content = "Должности"
	else:
		content = "Positions"
	return content


@register.simple_tag
def profile_add_new_position(lang=None):
	if lang == 'en':
		content = "Add new position"
	elif lang == 'tr':
		content = "Yeni pozisyon ekle"
	elif lang == 'bs':
		content = "Dodaj novu poziciju"
	elif lang == 'ru':
		content = "Добавить новую должность"
	else:
		content = "Add new position"
	return content


@register.simple_tag
def definition_add_new_user(lang=None):
	if lang == 'en':
		content = "Add new user"
	elif lang == 'tr':
		content = "Yeni kullanıcı ekle"
	elif lang == 'bs':
		content = "Dodaj novog korisnika"
	else:
		content = "Add new user"
	return content


@register.simple_tag
def definition_administrator(lang=None):
	if lang == 'en':
		content = "Administrator"
	elif lang == 'tr':
		content = "Yönetici"
	elif lang == 'bs':
		content = "Administrator"
	elif lang == 'ru':
		content = "Администратор"
	else:
		content = "Administrator"
	return content


@register.simple_tag
def definition_administrators(lang=None):
	if lang == 'en':
		content = "Administrators"
	elif lang == 'tr':
		content = "Yöneticiler"
	elif lang == 'bs':
		content = "Administratori"
	elif lang == 'ru':
		content = "Администраторы"
	else:
		content = "Administrators"
	return content


@register.simple_tag
def definition_add_new_administrator(lang=None):
	if lang == 'en':
		content = "Add new administrator"
	elif lang == 'tr':
		content = "Yeni yönetici ekle"
	elif lang == 'bs':
		content = "Dodaj novog administratora"
	else:
		content = "Add new administrator"
	return content


@register.simple_tag
def definition_user(lang=None):
	if lang == 'en':
		content = "User"
	elif lang == 'tr':
		content = "Kullanıcı"
	elif lang == 'bs':
		content = "Korisnik"
	elif lang == 'ru':
		content = "Пользователь"
	else:
		content = "User"
	return content


@register.simple_tag
def definition_users(lang=None):
	if lang == 'en':
		content = "Users"
	elif lang == 'tr':
		content = "Kullanıcılar"
	elif lang == 'bs':
		content = "Korisnici"
	elif lang == 'ru':
		content = "Пользователи"
	else:
		content = "Users"
	return content


@register.simple_tag
def definition_enabled(lang=None):
	if lang == 'en':
		content = "Enabled"
	elif lang == 'tr':
		content = "Etkin"
	elif lang == 'bs':
		content = "Uključeno"
	elif lang == 'ru':
		content = "Включен"
	else:
		content = "Enabled"
	return content


@register.simple_tag
def definition_disabled(lang=None):
	if lang == 'en':
		content = "Disabled"
	elif lang == 'tr':
		content = "Etkisiz"
	elif lang == 'bs':
		content = "Isključeno"
	elif lang == 'ru':
		content = "Отключен"
	else:
		content = "Disabled"
	return content


@register.simple_tag
def definition_active(lang=None):
	if lang == 'en':
		content = "Active"
	elif lang == 'tr':
		content = "Aktif"
	elif lang == 'bs':
		content = "Aktivno"
	elif lang == 'ru':
		content = "Активный"
	else:
		content = "Active"
	return content


@register.simple_tag
def definition_inactive(lang=None):
	if lang == 'en':
		content = "Inactive"
	elif lang == 'tr':
		content = "Pasif"
	elif lang == 'bs':
		content = "Neaktivno"
	elif lang == 'ru':
		content = "Не активный"
	else:
		content = "Inactive"
	return content


@register.simple_tag
def ticket_issue_title(lang=None):
	if lang == 'en':
		content = "Issue title"
	elif lang == 'tr':
		content = "Görev başlığı"
	elif lang == 'bs':
		content = "Naslov problema"
	elif lang == 'ru':
		content = "Заголовок задания"
	else:
		content = "Issue title"
	return content


@register.simple_tag
def ticket_issue_description(lang=None):
	if lang == 'en':
		content = "Issue description"
	elif lang == 'tr':
		content = "Görev açıklaması"
	elif lang == 'bs':
		content = "Opis problema"
	elif lang == 'ru':
		content = "Описание задания"
	else:
		content = "Issue description"
	return content


@register.simple_tag
def ticket_task_details(lang=None):
	if lang == 'en':
		content = "Task details"
	elif lang == 'tr':
		content = "Görev detayları"
	elif lang == 'bs':
		content = "Detalji o zadatku"
	elif lang == 'ru':
		content = "Описание задачи"
	else:
		content = "Task details"
	return content


@register.simple_tag
def general_attached_file(lang=None):
	if lang == 'en':
		content = "Attached file"
	elif lang == 'tr':
		content = "Eklenen dosya"
	elif lang == 'bs':
		content = "Privitak"
	elif lang == 'ru':
		content = "Прикрепленный файл"
	else:
		content = "Attached file"
	return content


@register.simple_tag
def general_attached_files(lang=None):
	if lang == 'en':
		content = "Attached files"
	elif lang == 'tr':
		content = "Eklenen dosyalar"
	elif lang == 'bs':
		content = "Privitci"
	elif lang == 'ru':
		content = "Прикрепленные файлы"
	else:
		content = "Attached files"
	return content


@register.simple_tag
def general_assigned_user(lang=None):
	if lang == 'en':
		content = "Assigned user"
	elif lang == 'tr':
		content = "İlgili kullanıcı"
	elif lang == 'bs':
		content = "Dodijeljeni korisnik"
	elif lang == 'ru':
		content = "Назначенный пользователь"
	else:
		content = "Assigned user"
	return content


@register.simple_tag
def general_assigned_users(lang=None):
	if lang == 'en':
		content = "Assigned users"
	elif lang == 'tr':
		content = "İlgili kullanıcılar"
	elif lang == 'bs':
		content = "Dodijeljeni korisnici"
	elif lang == 'ru':
		content = "Назначенные пользователи"
	else:
		content = "Assigned users"
	return content


@register.simple_tag
def general_save_issue_before_attaching_files(lang=None):
	if lang == 'en':
		content = "You have to save issue before attaching files"
	elif lang == 'tr':
		content = "Dosya eklemeden önce görevi kaydediniz"
	elif lang == 'bs':
		content = "Prije dodavanja privitka prvo morate spremiti problem"
	elif lang == 'ru':
		content = "Вам необходимо сохранить задание что бы добавить файл"
	else:
		content = "You have to save issue before attaching files"
	return content


@register.simple_tag
def general_project(lang=None):
	if lang == 'en':
		content = "Project"
	elif lang == 'tr':
		content = "Proje"
	elif lang == 'bs':
		content = "Projekat"
	elif lang == 'ru':
		content = "Проект"
	else:
		content = "Project"
	return content


@register.simple_tag
def general_priority(lang=None):
	if lang == 'en':
		content = "Priority"
	elif lang == 'tr':
		content = "Önem derecesi"
	elif lang == 'bs':
		content = "Prioritet"
	elif lang == 'ru':
		content = "Приоритет"
	else:
		content = "Priority"
	return content


@register.simple_tag
def general_added_by(lang=None):
	if lang == 'en':
		content = "Added by"
	elif lang == 'tr':
		content = "Ekleyen"
	elif lang == 'bs':
		content = "Dodano od"
	elif lang == 'ru':
		content = "Добавил(а)"
	else:
		content = "Added by"
	return content


@register.simple_tag
def general_start_date(lang=None):
	if lang == 'en':
		content = "Start date"
	elif lang == 'tr':
		content = "Başlangiç tarihi"
	elif lang == 'bs':
		content = "Datum početka"
	elif lang == 'ru':
		content = "Дата начала"
	else:
		content = "Start date"
	return content


@register.simple_tag
def general_end_date(lang=None):
	if lang == 'en':
		content = "End date"
	elif lang == 'tr':
		content = "Son tarihi"
	elif lang == 'bs':
		content = "Datum kraja"
	elif lang == 'ru':
		content = "Дата окончания"
	else:
		content = "End date"
	return content


@register.simple_tag
def general_due_date(lang=None):
	if lang == 'en':
		content = "Due date"
	elif lang == 'tr':
		content = "Bitiş tarihi"
	elif lang == 'bs':
		content = "Zadnji datum za izvršenje"
	elif lang == 'ru':
		content = "Дата завершения"
	else:
		content = "Due date"
	return content


@register.simple_tag
def general_publish_date(lang=None):
	if lang == 'en':
		content = "Publish date"
	elif lang == 'tr':
		content = "Yayınlanma tarihi"
	elif lang == 'bs':
		content = "Izdato datuma"
	elif lang == 'ru':
		content = "Дата публикации"
	else:
		content = "Publish date"
	return content


@register.simple_tag
def general_comment(lang=None):
	if lang == 'en':
		content = "Comment"
	elif lang == 'tr':
		content = "Yorum"
	elif lang == 'bs':
		content = "Komentar"
	elif lang == 'ru':
		content = "Комментарий"
	else:
		content = "Comment"
	return content


@register.simple_tag
def general_comments(lang=None):
	if lang == 'en':
		content = "Comments"
	elif lang == 'tr':
		content = "Yorumlar"
	elif lang == 'bs':
		content = "Komentari"
	elif lang == 'ru':
		content = "Комментарии"
	else:
		content = "Comments"
	return content


@register.simple_tag
def general_definition(lang=None):
	if lang == 'en':
		content = "Definition"
	elif lang == 'tr':
		content = "Tanım"
	elif lang == 'bs':
		content = "Definicija"
	elif lang == 'ru':
		content = "Обозначение"
	else:
		content = "Definition"
	return content


@register.simple_tag
def general_definitions(lang=None):
	if lang == 'en':
		content = "Definitions"
	elif lang == 'tr':
		content = "Tanımlar"
	elif lang == 'bs':
		content = "Definicije"
	elif lang == 'ru':
		content = "Обозначения"
	else:
		content = "Definitions"
	return content


@register.simple_tag
def general_css_class(lang=None):
	if lang == 'en':
		content = "CSS class"
	elif lang == 'tr':
		content = "Css sınıfı"
	elif lang == 'bs':
		content = "CSS klasa"
	elif lang == 'ru':
		content = "CSS класс"
	else:
		content = "CSS class"
	return content


@register.simple_tag
def general_add_new_label(lang=None):
	if lang == 'en':
		content = "Add new label"
	elif lang == 'tr':
		content = "Yeni etiket ekle"
	elif lang == 'bs':
		content = "Dodaj novu etiketu"
	elif lang == 'ru':
		content = "Добавить новую этикетку"
	else:
		content = "Add new label"
	return content


@register.simple_tag
def general_cargo_labels(lang=None):
	if lang == 'en':
		content = "Cargo labels"
	elif lang == 'tr':
		content = "Kargo etiketleri"
	elif lang == 'bs':
		content = "Kargo etikete"
	elif lang == 'ru':
		content = "Маркировка груза"
	else:
		content = "Cargo labels"
	return content


@register.simple_tag
def general_add_new_cargo_label(lang=None):
	if lang == 'en':
		content = "Add new cargo label"
	elif lang == 'tr':
		content = "Yeni kargo etiketi ekle"
	elif lang == 'bs':
		content = "Dodaj novu kargo etiketu"
	elif lang == 'ru':
		content = "Добавить новую маркировку груза"
	else:
		content = "Add new cargo label"
	return content


@register.simple_tag
def general_vehicle_type(lang=None):
	if lang == 'en':
		content = "Vehicle type"
	elif lang == 'tr':
		content = "Araç tipi"
	elif lang == 'bs':
		content = "Vrste vozila"
	elif lang == 'ru':
		content = "Типы автотранспорта"
	else:
		content = "Vehicle type"
	return content


@register.simple_tag
def general_destinations(lang=None):
	if lang == 'en':
		content = "Destinations"
	elif lang == 'tr':
		content = "Varış yeri"
	elif lang == 'bs':
		content = "Destinacije"
	elif lang == 'ru':
		content = "Назначеняи"
	else:
		content = "Destinations"
	return content


@register.simple_tag
def general_airport(lang=None):
	if lang == 'en':
		content = "Airport"
	elif lang == 'tr':
		content = "Havalimanı"
	elif lang == 'bs':
		content = "Aerodrom"
	elif lang == 'ru':
		content = "Аэропорт"
	else:
		content = "Airport"
	return content


@register.simple_tag
def general_airports(lang=None):
	if lang == 'en':
		content = "Airports"
	elif lang == 'tr':
		content = "Havaalanları"
	elif lang == 'bs':
		content = "Aerodromi"
	elif lang == 'ru':
		content = "Аэропорты"
	else:
		content = "Airports"
	return content


@register.simple_tag
def general_latitude(lang=None):
	if lang == 'en':
		content = "Latitude"
	elif lang == 'tr':
		content = "Enlem"
	elif lang == 'bs':
		content = "Širina (geo.)"
	elif lang == 'ru':
		content = "Широта"
	else:
		content = "Latitude"
	return content


@register.simple_tag
def general_longitude(lang=None):
	if lang == 'en':
		content = "Longitude"
	elif lang == 'tr':
		content = "Boylam"
	elif lang == 'bs':
		content = "Dužina (geo.)"
	elif lang == 'ru':
		content = "Долгота"
	else:
		content = "Longitude"
	return content


@register.simple_tag
def general_elevation(lang=None):
	if lang == 'en':
		content = "Elevation"
	elif lang == 'tr':
		content = "Yükseklik"
	elif lang == 'bs':
		content = "Nadmorska visina"
	elif lang == 'ru':
		content = "Подъем"
	else:
		content = "Elevation"
	return content


@register.simple_tag
def general_select_country(lang=None):
	if lang == 'en':
		content = "Select country"
	elif lang == 'tr':
		content = "Ülke seç"
	elif lang == 'bs':
		content = "Izaberi državu"
	elif lang == 'ru':
		content = "Выберите страну"
	else:
		content = "Select country"
	return content


@register.simple_tag
def general_select_state(lang=None):
	if lang == 'en':
		content = "Select state"
	elif lang == 'tr':
		content = "İl seç"
	elif lang == 'bs':
		content = "Izaberi provinciju"
	elif lang == 'ru':
		content = "Выберите область"
	else:
		content = "Select state"
	return content


@register.simple_tag
def general_select_city(lang=None):
	if lang == 'en':
		content = "Select city"
	elif lang == 'tr':
		content = "İlçe seç"
	elif lang == 'bs':
		content = "Izaberi grad"
	elif lang == 'ru':
		content = "Выберите город"
	else:
		content = "Select city"
	return content


@register.simple_tag
def general_seaport(lang=None):
	if lang == 'en':
		content = "Seaport"
	elif lang == 'tr':
		content = "Liman"
	elif lang == 'bs':
		content = "Luka"
	elif lang == 'ru':
		content = "Порт"
	else:
		content = "Seaport"
	return content


@register.simple_tag
def general_seaports(lang=None):
	if lang == 'en':
		content = "Seaports"
	elif lang == 'tr':
		content = "Limanlar"
	elif lang == 'bs':
		content = "Luke"
	elif lang == 'ru':
		content = "Порты"
	else:
		content = "Seaports"
	return content


@register.simple_tag
def general_train_station(lang=None):
	if lang == 'en':
		content = "Train station"
	elif lang == 'tr':
		content = "Tren istasyonu"
	elif lang == 'bs':
		content = "Željeznička stanica"
	elif lang == 'ru':
		content = "ЖД станция"
	else:
		content = "Train station"
	return content


@register.simple_tag
def general_train_stations(lang=None):
	if lang == 'en':
		content = "Train stations"
	elif lang == 'tr':
		content = "Tren istasyonları"
	elif lang == 'bs':
		content = "Željezničke stanice"
	elif lang == 'ru':
		content = "ЖД станции"
	else:
		content = "Train stations"
	return content


@register.simple_tag
def general_add_new_airport(lang=None):
	if lang == 'en':
		content = "Add new airport"
	elif lang == 'tr':
		content = "Yeni havalimanı ekle"
	elif lang == 'bs':
		content = "Dodaj novi aerodrom"
	elif lang == 'ru':
		content = "Добавить новый аэропорт"
	else:
		content = "Add new airport"
	return content


@register.simple_tag
def general_add_new_seaport(lang=None):
	if lang == 'en':
		content = "Add new seaport"
	elif lang == 'tr':
		content = "Yeni liman ekle"
	elif lang == 'bs':
		content = "Dodaj novu luku"
	elif lang == 'ru':
		content = "Добавить новый порт"
	else:
		content = "Add new seaport"
	return content


@register.simple_tag
def general_add_new_train_station(lang=None):
	if lang == 'en':
		content = "Add new train station"
	elif lang == 'tr':
		content = "Yeni tren istasyonu ekle"
	elif lang == 'bs':
		content = "Dodaj novu željezničku stanicu"
	elif lang == 'ru':
		content = "Добавить ЖД станцию"
	else:
		content = "Add new train station"
	return content


@register.simple_tag
def general_custom(lang=None):
	if lang == 'en':
		content = "Custom"
	elif lang == 'tr':
		content = "Gümrük"
	elif lang == 'bs':
		content = "Carina"
	elif lang == 'ru':
		content = "Таможня"
	else:
		content = "Custom"
	return content


@register.simple_tag
def general_customs(lang=None):
	if lang == 'en':
		content = "Customs"
	elif lang == 'tr':
		content = "Gümrükler"
	elif lang == 'bs':
		content = "Carine"
	elif lang == 'ru':
		content = "Таможни"
	else:
		content = "Customs"
	return content


@register.simple_tag
def general_add_new_custom(lang=None):
	if lang == 'en':
		content = "Add new custom"
	elif lang == 'tr':
		content = "Yeni gümrük ekle"
	elif lang == 'bs':
		content = "Dodaj novu carinu"
	elif lang == 'ru':
		content = "Создать новое задание"
	else:
		content = "Add new custom"
	return content


@register.simple_tag
def general_finance_management(lang=None):
	if lang == 'en':
		content = "Finance management"
	elif lang == 'tr':
		content = "Finans yönetimi"
	elif lang == 'bs':
		content = "Finansijski menadžer"
	elif lang == 'ru':
		content = "Управление финансами"
	else:
		content = "Finance management"
	return content


@register.simple_tag
def general_central_bank(lang=None):
	if lang == 'en':
		content = "Central bank"
	elif lang == 'tr':
		content = "Merkez bankası"
	elif lang == 'bs':
		content = "Centralna banka"
	elif lang == 'ru':
		content = "Центральный банк"
	else:
		content = "Central bank"
	return content


@register.simple_tag
def general_central_banks(lang=None):
	if lang == 'en':
		content = "Central banks"
	elif lang == 'tr':
		content = "Merkez bankaları"
	elif lang == 'bs':
		content = "Centralne banke"
	elif lang == 'ru':
		content = "Центральные банки"
	else:
		content = "Central banks"
	return content


@register.simple_tag
def general_add_new_central_bank(lang=None):
	if lang == 'en':
		content = "Add new central bank"
	elif lang == 'tr':
		content = "Yeni merkez bankası ekle"
	elif lang == 'bs':
		content = "Dodaj novu centralnu banku"
	else:
		content = "Add new central bank"
	return content


@register.simple_tag
def general_currency(lang=None):
	if lang == 'en':
		content = "Currency"
	elif lang == 'tr':
		content = "Para birimi"
	elif lang == 'bs':
		content = "Valuta"
	elif lang == 'ru':
		content = "Валюта"
	else:
		content = "Currency"
	return content


@register.simple_tag
def general_currencies(lang=None):
	if lang == 'en':
		content = "Currencies"
	elif lang == 'tr':
		content = "Kurlar"
	elif lang == 'bs':
		content = "Valute"
	elif lang == 'ru':
		content = "Валюты"
	else:
		content = "Currencies"
	return content


@register.simple_tag
def general_available_currencies(lang=None):
	if lang == 'en':
		content = "Available currencies"
	elif lang == 'tr':
		content = "Mevcut para birimleri"
	elif lang == 'bs':
		content = "Dostupne valute"
	elif lang == 'ru':
		content = "Доступные валюты"
	else:
		content = "Available currencies"
	return content


@register.simple_tag
def general_add_new_currency(lang=None):
	if lang == 'en':
		content = "Add new currency"
	elif lang == 'tr':
		content = "Yeni para birimi ekle"
	elif lang == 'bs':
		content = "Dodaj novu valutu"
	elif lang == 'ru':
		content = "Добавить новую валюту"
	else:
		content = "Add new currency"
	return content


@register.simple_tag
def general_numeric_code(lang=None):
	if lang == 'en':
		content = "Numeric code"
	elif lang == 'tr':
		content = "Sayısal kod"
	elif lang == 'bs':
		content = "Brojčani kod"
	elif lang == 'ru':
		content = "Нумеричный код"
	else:
		content = "Numeric code"
	return content


@register.simple_tag
def general_code_alpha(lang=None):
	if lang == 'en':
		content = "Code alpha"
	elif lang == 'tr':
		content = "Alfa kod"
	elif lang == 'bs':
		content = "Kod alfa"
	elif lang == 'ru':
		content = "Альфа код"
	else:
		content = "Code alpha"
	return content


@register.simple_tag
def general_select_central_bank(lang=None):
	if lang == 'en':
		content = "Select central bank"
	elif lang == 'tr':
		content = "Merkez bankasını seçin"
	elif lang == 'bs':
		content = "Izaberi centralnu banku"
	elif lang == 'ru':
		content = "Выберите центральный банк"
	else:
		content = "Select central bank"
	return content


@register.simple_tag
def general_select_date(lang=None):
	if lang == 'en':
		content = "Select date"
	elif lang == 'tr':
		content = "Tarih seç"
	elif lang == 'bs':
		content = "Izaberi datum"
	elif lang == 'ru':
		content = "Веберите дату"
	else:
		content = "Select date"
	return content


@register.simple_tag
def general_show_exchange_rates(lang=None):
	if lang == 'en':
		content = "Show exchange rates"
	elif lang == 'tr':
		content = "Döviz kurlarını göster"
	elif lang == 'bs':
		content = "Prikaži prodajni kurs"
	elif lang == 'ru':
		content = "Показать курсы валют"
	else:
		content = "Show exchange rates"
	return content


@register.simple_tag
def general_currency_pair(lang=None):
	if lang == 'en':
		content = "Currency pair"
	elif lang == 'tr':
		content = "Döviz çifti"
	elif lang == 'bs':
		content = "Par valuta"
	elif lang == 'ru':
		content = "Пара"
	else:
		content = "Currency pair"
	return content


@register.simple_tag
def general_unit(lang=None):
	if lang == 'en':
		content = "Unit"
	elif lang == 'tr':
		content = "Birim"
	elif lang == 'bs':
		content = "Jedinica"
	elif lang == 'ru':
		content = "Единица"
	else:
		content = "Unit"
	return content


@register.simple_tag
def general_buying(lang=None):
	if lang == 'en':
		content = "Buying"
	elif lang == 'tr':
		content = "Alış"
	elif lang == 'bs':
		content = "Kupovina"
	elif lang == 'ru':
		content = "Покупка"
	else:
		content = "Buying"
	return content


@register.simple_tag
def general_selling(lang=None):
	if lang == 'en':
		content = "Selling"
	elif lang == 'tr':
		content = "Satış"
	elif lang == 'bs':
		content = "Prodaja"
	elif lang == 'ru':
		content = "Продажа"
	else:
		content = "Selling"
	return content


@register.simple_tag
def general_active_issues(lang=None):
	if lang == 'en':
		content = "Active issues"
	elif lang == 'tr':
		content = "Aktif görevler"
	elif lang == 'bs':
		content = "Aktivni problemi"
	elif lang == 'ru':
		content = "Активные задания"
	else:
		content = "Active issues"
	return content


@register.simple_tag
def general_closed_issues(lang=None):
	if lang == 'en':
		content = "Closed issues"
	elif lang == 'tr':
		content = "Kapalı görevler"
	elif lang == 'bs':
		content = "Zatvoreni problemi"
	elif lang == 'ru':
		content = "Завершенные задания"
	else:
		content = "Closed issues"
	return content


@register.simple_tag
def general_create_new_issue(lang=None):
	if lang == 'en':
		content = "Create new issue"
	elif lang == 'tr':
		content = "Yeni görev oluştur"
	elif lang == 'bs':
		content = "Napravi novi problem"
	elif lang == 'ru':
		content = "Создать новое задание"
	else:
		content = "Create new issue"
	return content


@register.simple_tag
def general_currency_exchange_rates(lang=None):
	if lang == 'en':
		content = "Currency exchange rates"
	elif lang == 'tr':
		content = "Döviz kuru"
	elif lang == 'bs':
		content = "Trenutni prodajni kurs"
	elif lang == 'ru':
		content = "Курсы обмена валют"
	else:
		content = "Currency exchange rates"
	return content


@register.simple_tag
def general_exchange_rates_are_not_found(lang=None):
	if lang == 'en':
		content = "Exchange rates are not found"
	elif lang == 'tr':
		content = "Döviz kurları bulunamadı"
	elif lang == 'bs':
		content = "Prodajni kurs nije pronađen"
	else:
		content = "Exchange rates are not found"
	return content


@register.simple_tag
def general_cancel_changes(lang=None):
	if lang == 'en':
		content = "Cancel changes"
	elif lang == 'tr':
		content = "Değişikleri iptal et"
	elif lang == 'bs':
		content = "Otkaži promjene"
	elif lang == 'ru':
		content = "Отменить изменения"
	else:
		content = "Cancel changes"
	return content


@register.simple_tag
def general_type_to_filter(lang=None):
	if lang == 'en':
		content = "Type to filter"
	elif lang == 'tr':
		content = "Filtrelemek için yazın"
	elif lang == 'bs':
		content = "Unesi za filtriranje"
	elif lang == 'ru':
		content = "Введите текст для поиска"
	else:
		content = "Type to filter"
	return content


@register.simple_tag
def ticket_active_tickets(lang=None):
	if lang == 'en':
		content = "Active tickets"
	elif lang == 'tr':
		content = "Etkin ticket"
	elif lang == 'bs':
		content = "Aktivni tiketi"
	elif lang == 'ru':
		content = "Активные тикеты"
	else:
		content = "Active tickets"
	return content


@register.simple_tag
def ticket_closed_tickets(lang=None):
	if lang == 'en':
		content = "Closed tickets"
	elif lang == 'tr':
		content = "Kapalı ticket"
	elif lang == 'bs':
		content = "Zatvoreni tiketi"
	elif lang == 'ru':
		content = "Закрытые тикеты"
	else:
		content = "Closed tickets"
	return content


@register.simple_tag
def ticket_create_new_ticket(lang=None):
	if lang == 'en':
		content = "Create new ticket"
	elif lang == 'tr':
		content = "Yeni ticket oluşturma"
	elif lang == 'bs':
		content = "Napravi novi tiket"
	elif lang == 'ru':
		content = "Открыть новый тикет"
	else:
		content = "Create new ticket"
	return content


@register.simple_tag
def general_created(lang=None):
	if lang == 'en':
		content = "Created"
	elif lang == 'tr':
		content = "Oluşturuldu"
	elif lang == 'bs':
		content = "Stvoreno"
	elif lang == 'ru':
		content = "Создан"
	else:
		content = "Created"
	return content


@register.simple_tag
def general_additional_information(lang=None):
	if lang == 'en':
		content = "Additional information"
	elif lang == 'tr':
		content = "Ek bilgi"
	elif lang == 'bs':
		content = "Dodatna informacija"
	elif lang == 'ru':
		content = "Дополнительная информация"
	else:
		content = "Additional information"
	return content


@register.simple_tag
def general_general_information(lang=None):
	if lang == 'en':
		content = "General information"
	elif lang == 'tr':
		content = "Genel bilgiler"
	elif lang == 'bs':
		content = "Generalna informacija"
	elif lang == 'ru':
		content = "Основная информация"
	else:
		content = "General information"
	return content


@register.simple_tag
def company_company_title(lang=None):
	if lang == 'en':
		content = "Company title"
	elif lang == 'tr':
		content = "Şirket unvanı"
	elif lang == 'bs':
		content = "Naslov kompanije"
	elif lang == 'ru':
		content = "Название компании"
	else:
		content = "Company title"
	return content


@register.simple_tag
def company_sales_manager(lang=None):
	if lang == 'en':
		content = "Sales manager"
	elif lang == 'tr':
		content = "Satış Müdürü"
	elif lang == 'bs':
		content = "Menadžer prodaje"
	elif lang == 'ru':
		content = "Управляющий продажей"
	else:
		content = "Sales manager"
	return content


@register.simple_tag
def company_operations_manager(lang=None):
	if lang == 'en':
		content = "Operations manager"
	elif lang == 'tr':
		content = "Operasyon Müdürü"
	elif lang == 'bs':
		content = "Menadžer operacija"
	elif lang == 'ru':
		content = "Управляющий операциями"
	else:
		content = "Operations manager"
	return content


@register.simple_tag
def company_company_code(lang=None):
	if lang == 'en':
		content = "Company code"
	elif lang == 'tr':
		content = "Şirket kodu"
	elif lang == 'bs':
		content = "Kod kompanije"
	elif lang == 'ru':
		content = "Код компании"
	else:
		content = "Company code"
	return content


@register.simple_tag
def ticket_enter_comment(lang=None):
	if lang == 'en':
		content = "Enter comment"
	elif lang == 'tr':
		content = "Yorum gir"
	elif lang == 'bs':
		content = "Unesi komentar"
	elif lang == 'ru':
		content = "Введите коментарий"
	else:
		content = "Enter comment"
	return content


@register.simple_tag
def ticket_add_comment(lang=None):
	if lang == 'en':
		content = "Add comment"
	elif lang == 'tr':
		content = "Yorum ekle"
	elif lang == 'bs':
		content = "Komentariši"
	elif lang == 'ru':
		content = "Добавить комментарий"
	else:
		content = "Add comment"
	return content


@register.simple_tag
def ticket_no_active_issues_found(lang=None):
	if lang == 'en':
		content = "No active issues found"
	elif lang == 'tr':
		content = "Etkin bir görev bulunamadı"
	elif lang == 'bs':
		content = "Nije pronađen nijedan otvoren problem"
	elif lang == 'ru':
		content = "Нет активных задач"
	else:
		content = "No active issues found"
	return content


@register.simple_tag
def ticket_no_closed_issues_found(lang=None):
	if lang == 'en':
		content = "No closed issues found"
	elif lang == 'tr':
		content = "Kapalı görev bulunamadı"
	elif lang == 'bs':
		content = "Nije pronađen nijedan zatvoren problem"
	elif lang == 'ru':
		content = "Нет завершенных задач"
	else:
		content = "No closed issues found"
	return content


@register.simple_tag
def general_created_on(lang=None):
	if lang == 'en':
		content = "Created on"
	elif lang == 'tr':
		content = "Oluşturulan"
	elif lang == 'bs':
		content = "Stvoreno"
	elif lang == 'ru':
		content = "Создан"
	else:
		content = "Created on"
	return content


@register.simple_tag
def general_size(lang=None):
	if lang == 'en':
		content = "Size"
	elif lang == 'tr':
		content = "Boyut"
	elif lang == 'bs':
		content = "Veličina"
	elif lang == 'ru':
		content = "Размер"
	else:
		content = "Size"
	return content


@register.simple_tag
def ticket_no_active_tickets_found(lang=None):
	if lang == 'en':
		content = "No active tickets found"
	elif lang == 'tr':
		content = "Aktif bilet bulunamadı"
	elif lang == 'bs':
		content = "Nije pronađen nijedan otvoren tiket"
	elif lang == 'ru':
		content = "Нет открытых тикетов"
	else:
		content = "No active tickets found"
	return content


@register.simple_tag
def ticket_no_closed_tickets_found(lang=None):
	if lang == 'en':
		content = "No closed tickets found"
	elif lang == 'tr':
		content = "Kapalı bilet bulunamadı"
	elif lang == 'bs':
		content = "Nije pronađen nijedan zatvoren tiket"
	elif lang == 'ru':
		content = "Нет закрытых тикетов"
	else:
		content = "No closed tickets found"
	return content


@register.simple_tag
def company_add_client_company(lang=None):
	if lang == 'en':
		content = "Add Client Company "
	elif lang == 'tr':
		content = "İstemci şirketi ekle"
	elif lang == 'bs':
		content = "Dodaj klijent kompaniju"
	elif lang == 'ru':
		content = "Добавить компанию клиента"
	else:
		content = "Add Client Company "
	return content


@register.simple_tag
def company_client_company(lang=None):
	if lang == 'en':
		content = "Client company"
	elif lang == 'tr':
		content = "Müşteri şirketi"
	elif lang == 'bs':
		content = "Klijent kompanija"
	elif lang == 'ru':
		content = "Компания клиента"
	else:
		content = "Client company"
	return content


@register.simple_tag
def ticket_close_issue(lang=None):
	if lang == 'en':
		content = "Close issue"
	elif lang == 'tr':
		content = "Görev kapalı"
	elif lang == 'bs':
		content = "Zatvori problem"
	elif lang == 'ru':
		content = "Закрыть задание"
	else:
		content = "Close issue"
	return content


@register.simple_tag
def ticket_close_ticket(lang=None):
	if lang == 'en':
		content = "Close ticket"
	elif lang == 'tr':
		content = "Ticket kapalı"
	elif lang == 'bs':
		content = "Zatvori tiket"
	elif lang == 'ru':
		content = "Закрыть тикет"
	else:
		content = "Close ticket"
	return content


@register.simple_tag
def ticket_main_issue(lang=None):
	if lang == 'en':
		content = "Main issue"
	elif lang == 'tr':
		content = "Ana görev"
	elif lang == 'bs':
		content = "Glavni problem"
	elif lang == 'ru':
		content = "Основное задание"
	else:
		content = "Main issue"
	return content


@register.simple_tag
def ticket_are_you_sure_you_want_to_close_ticket(lang=None):
	if lang == 'en':
		content = "Are you sure you want to close ticket?"
	elif lang == 'tr':
		content = "Ticket kapatmak istediğine emin misin?"
	elif lang == 'bs':
		content = "Da li ste sigurni da želite zatvoriti tiket?"
	elif lang == 'ru':
		content = "Вы уверены, что хотите закрыть тикет?"
	else:
		content = "Are you sure you want to close ticket?"
	return content


@register.simple_tag
def ticket_are_you_sure_you_want_to_close_issue(lang=None):
	if lang == 'en':
		content = "Are you sure you want to close issue?"
	elif lang == 'tr':
		content = "Görevi kapatmak istediğine emin misin?"
	elif lang == 'bs':
		content = "Da li ste sigurni da želite zatvoriti problem?"
	elif lang == 'ru':
		content = "Вы уверены, что хотите закрыть задание?"
	else:
		content = "Are you sure you want to close issue?"
	return content


@register.simple_tag
def ticket_if_you_close_issue_you_will_not_be_able_to_edit_it_no_more_and_it_will_stay_closed_permanently(lang=None):
	if lang == 'en':
		content = "If you close issue you will not be able to edit it no more, and it will stay closed permanently"
	elif lang == 'tr':
		content = "Eğer görevi kapatırsanız artık düzenlemek mümkün olmayacaktır, ve kalıcı olarak kapalı kalacaktır"
	elif lang == 'ru':
		content = "Если вы закроете задание, то Вы больше не сможете его редактировать, и оно останется закрытым навсегда"
	else:
		content = "If you close issue you will not be able to edit it no more, and it will stay closed permanently"
	return content


@register.simple_tag
def ticket_if_you_close_ticket_you_will_not_be_able_to_edit_it_no_more_and_it_will_stay_closed_permanently(lang=None):
	if lang == 'en':
		content = " If you close ticket you will not be able to edit it no more, and it will stay closed permanently "
	elif lang == 'bs':
		content = "Ako zatvorite tiket, nećete više biti u mogućnosti uređivati ga i on će ostati zatvoren trajno"
	else:
		content = " If you close ticket you will not be able to edit it no more, and it will stay closed permanently "
	return content


@register.simple_tag
def general_department(lang=None):
	if lang == 'en':
		content = "Department"
	elif lang == 'tr':
		content = "Departman"
	elif lang == 'bs':
		content = "Odsjek"
	elif lang == 'ru':
		content = "Отдел"
	else:
		content = "Department"
	return content


@register.simple_tag
def general_departments(lang=None):
	if lang == 'en':
		content = "Departments"
	elif lang == 'tr':
		content = "Departmanlar"
	elif lang == 'bs':
		content = "Odsjeci"
	elif lang == 'ru':
		content = "Отделы"
	else:
		content = "Departments"
	return content


@register.simple_tag
def ticket_support_department(lang=None):
	if lang == 'en':
		content = "Support department"
	elif lang == 'tr':
		content = "Destek departmanı"
	elif lang == 'bs':
		content = "Odjel za podršku"
	elif lang == 'ru':
		content = "Отдел поддержки"
	else:
		content = "Support department"
	return content


@register.simple_tag
def ticket_support_departments(lang=None):
	if lang == 'en':
		content = "Support departments"
	elif lang == 'tr':
		content = "Destek departmanı"
	elif lang == 'bs':
		content = "Odjeli za podršku"
	elif lang == 'ru':
		content = "Отделы поддержки"
	else:
		content = "Support departments"
	return content


@register.simple_tag
def general_notifications(lang=None):
	if lang == 'en':
		content = "Notifications"
	elif lang == 'tr':
		content = "Bildirimler"
	elif lang == 'bs':
		content = "Obavijesti"
	elif lang == 'ru':
		content = "Уведомления"
	else:
		content = "Notifications"
	return content


@register.simple_tag
def general_notification(lang=None):
	if lang == 'en':
		content = "Notification"
	elif lang == 'tr':
		content = "Bildirim"
	elif lang == 'bs':
		content = "Obavijest"
	elif lang == 'ru':
		content = "Уведомление"
	else:
		content = "Notification"
	return content


@register.simple_tag
def ticket_ticket_title(lang=None):
	if lang == 'en':
		content = "Ticket title"
	elif lang == 'tr':
		content = "Ticket başlığı"
	elif lang == 'bs':
		content = "Naslov tiketa"
	else:
		content = "Ticket title"
	return content


@register.simple_tag
def ticket_ticket_description(lang=None):
	if lang == 'en':
		content = "Ticket description"
	elif lang == 'tr':
		content = "Ticket açıklaması"
	elif lang == 'bs':
		content = "Opis tiketa"
	else:
		content = "Ticket description"
	return content


@register.simple_tag
def ticket_status(lang=None):
	if lang == 'en':
		content = "Status"
	elif lang == 'tr':
		content = "Durum"
	elif lang == 'bs':
		content = "Status"
	else:
		content = "Status"
	return content


@register.simple_tag
def ticket_ticket_type(lang=None):
	if lang == 'en':
		content = "Ticket Type"
	elif lang == 'tr':
		content = "Ticket Tipi"
	elif lang == 'bs':
		content = "Vrsta tiketa"
	else:
		content = "Ticket Type"
	return content


@register.simple_tag
def ticket_additional_information(lang=None):
	if lang == 'en':
		content = "Additional information"
	elif lang == 'tr':
		content = "Ek bilgi"
	elif lang == 'bs':
		content = "Dodatne informacije"
	else:
		content = "Additional information"
	return content


@register.simple_tag
def company_main_office(lang=None):
	if lang == 'en':
		content = "Main office"
	elif lang == 'bs':
		content = "Sjedište"
	else:
		content = "Main office"
	return content


@register.simple_tag
def general_general(lang=None):
	if lang == 'en':
		content = "General"
	elif lang == 'bs':
		content = "Općenito"
	else:
		content = "General"
	return content


@register.simple_tag
def general_general_informations(lang=None):
	if lang == 'en':
		content = "General informations"
	elif lang == 'bs':
		content = "Opće informacije"
	else:
		content = "General informations"
	return content


@register.simple_tag
def general_address_information(lang=None):
	if lang == 'en':
		content = "Address information"
	elif lang == 'bs':
		content = "Informacija o adresi"
	else:
		content = "Address information"
	return content


@register.simple_tag
def general_address_informations(lang=None):
	if lang == 'en':
		content = "Address informations"
	elif lang == 'bs':
		content = "Informacije o adresi"
	else:
		content = "Address informations"
	return content


@register.simple_tag
def general_note(lang=None):
	if lang == 'en':
		content = "Note"
	elif lang == 'bs':
		content = "Bilješka"
	else:
		content = "Note"
	return content


@register.simple_tag
def general_notes(lang=None):
	if lang == 'en':
		content = "Notes"
	elif lang == 'bs':
		content = "Bilješke"
	else:
		content = "Notes"
	return content


@register.simple_tag
def general_contact(lang=None):
	if lang == 'en':
		content = "Contact"
	elif lang == 'bs':
		content = "Kontakt"
	else:
		content = "Contact"
	return content


@register.simple_tag
def general_contacts(lang=None):
	if lang == 'en':
		content = "Contacts"
	elif lang == 'bs':
		content = "Kontakti"
	else:
		content = "Contacts"
	return content


@register.simple_tag
def profile_contact_informations(lang=None):
	if lang == 'en':
		content = "Contact informations"
	elif lang == 'bs':
		content = "Kontakt informacije"
	else:
		content = "Contact informations"
	return content


@register.simple_tag
def company_staff_information(lang=None):
	if lang == 'en':
		content = "Staff information"
	elif lang == 'bs':
		content = "Informacija osoblja"
	else:
		content = "Staff information"
	return content


@register.simple_tag
def company_staff_informations(lang=None):
	if lang == 'en':
		content = "Staff informations"
	elif lang == 'bs':
		content = "Informacije osoblja"
	else:
		content = "Staff informations"
	return content


@register.simple_tag
def company_is_branch_of(lang=None):
	if lang == 'en':
		content = "Is branch of"
	elif lang == 'bs':
		content = "Filijala od"
	else:
		content = "Is branch of"
	return content


@register.simple_tag
def general_zip_code(lang=None):
	if lang == 'en':
		content = "Zip code"
	elif lang == 'bs':
		content = "Poštanski broj"
	else:
		content = "Zip code"
	return content


@register.simple_tag
def company_address(lang=None):
	if lang == 'en':
		content = "Address"
	elif lang == 'bs':
		content = "Adresa"
	else:
		content = "Address"
	return content


@register.simple_tag
def company_static_phone_number(lang=None):
	if lang == 'en':
		content = "Static phone number"
	elif lang == 'bs':
		content = "Broj fiksnog telefona"
	else:
		content = "Static phone number"
	return content


@register.simple_tag
def company_website(lang=None):
	if lang == 'en':
		content = "Website"
	elif lang == 'bs':
		content = "Web stranica"
	else:
		content = "Website"
	return content


@register.simple_tag
def company_finance(lang=None):
	if lang == 'en':
		content = "Finance"
	elif lang == 'bs':
		content = "Finansije"
	else:
		content = "Finance"
	return content


@register.simple_tag
def company_bill_title(lang=None):
	if lang == 'en':
		content = "Bill title"
	elif lang == 'bs':
		content = "Naziv računa"
	else:
		content = "Bill title"
	return content


@register.simple_tag
def company_account_code(lang=None):
	if lang == 'en':
		content = "Account code"
	elif lang == 'bs':
		content = "Kod računa"
	else:
		content = "Account code"
	return content


@register.simple_tag
def company_eori_number(lang=None):
	if lang == 'en':
		content = "EORI number"
	elif lang == 'bs':
		content = "EORI broj"
	else:
		content = "EORI number"
	return content


@register.simple_tag
def company_tax_office(lang=None):
	if lang == 'en':
		content = "Tax office"
	elif lang == 'bs':
		content = "Sjedište porezne uprave"
	else:
		content = "Tax office"
	return content


@register.simple_tag
def company_tax_number(lang=None):
	if lang == 'en':
		content = "Tax number"
	elif lang == 'bs':
		content = "Broj poreznog računa"
	else:
		content = "Tax number"
	return content


@register.simple_tag
def company_special_code(lang=None):
	if lang == 'en':
		content = "Special code"
	elif lang == 'bs':
		content = "Specijalni kod"
	else:
		content = "Special code"
	return content


@register.simple_tag
def company_special_group_code(lang=None):
	if lang == 'en':
		content = "Special group code"
	elif lang == 'bs':
		content = "Specijalni grupni kod"
	else:
		content = "Special group code"
	return content


@register.simple_tag
def company_bank_name(lang=None):
	if lang == 'en':
		content = "Bank name"
	elif lang == 'bs':
		content = "Ime banke"
	else:
		content = "Bank name"
	return content


@register.simple_tag
def company_branch_name(lang=None):
	if lang == 'en':
		content = "Branch name"
	elif lang == 'bs':
		content = "Ime filijale"
	else:
		content = "Branch name"
	return content


@register.simple_tag
def company_branch_code(lang=None):
	if lang == 'en':
		content = "Branch code"
	elif lang == 'bs':
		content = "Kod filijale"
	else:
		content = "Branch code"
	return content


@register.simple_tag
def company_account_number(lang=None):
	if lang == 'en':
		content = "Account number"
	elif lang == 'tr':
		content = "Hesap numarası"
	elif lang == 'bs':
		content = "Broj računa"
	else:
		content = "Account number"
	return content


@register.simple_tag
def company_iban_number(lang=None):
	if lang == 'en':
		content = "IBAN number"
	elif lang == 'tr':
		content = "IBAN numarası"
	elif lang == 'bs':
		content = "IBAN broj"
	else:
		content = "IBAN number"
	return content


@register.simple_tag
def general_labels_high(lang=None):
	if lang == 'en':
		content = "High"
	elif lang == 'tr':
		content = "Yüksek"
	elif lang == 'bs':
		content = "Visoko"
	else:
		content = "High"
	return content


@register.simple_tag
def general_labels_extra_high(lang=None):
	if lang == 'en':
		content = "Extra high"
	elif lang == 'bs':
		content = "Veoma visoko"
	else:
		content = "Extra high"
	return content


@register.simple_tag
def general_labels_low(lang=None):
	if lang == 'en':
		content = "Low"
	elif lang == 'bs':
		content = "Nisko"
	else:
		content = "Low"
	return content


@register.simple_tag
def general_labels_normal(lang=None):
	if lang == 'en':
		content = "Normal"
	elif lang == 'bs':
		content = "Normalno"
	else:
		content = "Normal"
	return content


@register.simple_tag
def general_labels_above_normal(lang=None):
	if lang == 'en':
		content = "Above normal"
	elif lang == 'bs':
		content = "Iznad normalnog"
	else:
		content = "Above normal"
	return content


@register.simple_tag
def profile_registration_date(lang=None):
	if lang == 'en':
		content = "Registration date"
	elif lang == 'bs':
		content = "Datum registracije"
	else:
		content = "Registration date"
	return content


@register.simple_tag
def profile_full_name(lang=None):
	if lang == 'en':
		content = "Full name"
	elif lang == 'bs':
		content = "Puno ime i prezime"
	else:
		content = "Full name"
	return content


@register.simple_tag
def profile_sent_by_user(lang=None):
	if lang == 'en':
		content = "Sent by user"
	elif lang == 'bs':
		content = "Poslano od korisnika"
	else:
		content = "Sent by user"
	return content


@register.simple_tag
def general_status(lang=None):
	if lang == 'en':
		content = "Status"
	elif lang == 'tr':
		content = "Durum"
	elif lang == 'bs':
		content = "Status"
	else:
		content = "Status"
	return content


@register.simple_tag
def profile_invitation_date(lang=None):
	if lang == 'en':
		content = "Invitation date"
	elif lang == 'bs':
		content = "Datum pozivnice"
	else:
		content = "Invitation date"
	return content


@register.simple_tag
def general_is_main_office(lang=None):
	if lang == 'en':
		content = "Is main office"
	elif lang == 'bs':
		content = "Sjedište"
	else:
		content = "Is main office"
	return content


@register.simple_tag
def general_web_site(lang=None):
	if lang == 'en':
		content = "Website"
	elif lang == 'bs':
		content = "Web stranica"
	else:
		content = "Website"
	return content


@register.simple_tag
def company_delete_branch(lang=None):
	if lang == 'en':
		content = "Delete branch"
	elif lang == 'bs':
		content = "Izbriši filijalu"
	else:
		content = "Delete branch"
	return content


@register.simple_tag
def company_add_branch(lang=None):
	if lang == 'en':
		content = "Add new branch"
	elif lang == 'bs':
		content = "Dodaj novu filijalu"
	else:
		content = "Add new branch"
	return content


@register.simple_tag
def company_edit_branch(lang=None):
	if lang == 'en':
		content = "Edit branch"
	elif lang == 'bs':
		content = "Uredi filijalu"
	else:
		content = "Edit branch"
	return content


@register.simple_tag
def access_group_test_user(lang=None):
	if lang == 'en':
		content = "Test user"
	elif lang == 'bs':
		content = "Testiranje korisnika"
	else:
		content = "Test user"
	return content


@register.simple_tag
def general_labels_very_low(lang=None):
	if lang == 'en':
		content = "Very low"
	elif lang == 'tr':
		content = "Çok düşük"
	elif lang == 'bs':
		content = "Veoma nisko"
	elif lang == 'ru':
		content = "Очень низкая"
	else:
		content = "Very low"
	return content


@register.simple_tag
def container_types_hard_top(lang=None):
	if lang == 'en':
		content = "Hard Top"
	else:
		content = "Hard Top"
	return content


@register.simple_tag
def cargo_labels_fragile_handle_with_care(lang=None):
	if lang == 'en':
		content = "Fragile, Handle with care"
	else:
		content = "Fragile, Handle with care"
	return content


@register.simple_tag
def cargo_labels_use_no_hooks(lang=None):
	if lang == 'en':
		content = "Use no hooks"
	else:
		content = "Use no hooks"
	return content


@register.simple_tag
def cargo_labels_protect_from_heat_and_radioactive_sources(lang=None):
	if lang == 'en':
		content = "Protect from heat and radioactive sources"
	else:
		content = "Protect from heat and radioactive sources"
	return content


@register.simple_tag
def general_client_persons(lang=None):
	if lang == 'en':
		content = "Client Persons"
	else:
		content = "Client Persons"
	return content


@register.simple_tag
def general_add_client_person(lang=None):
	if lang == 'en':
		content = "Add Client Person"
	else:
		content = "Add Client Person"
	return content


@register.simple_tag
def profile_middle_name(lang=None):
	if lang == 'en':
		content = "Middle Name"
	else:
		content = "Middle Name"
	return content


@register.simple_tag
def cargo_labels_stacking_limitation_10(lang=None):
	if lang == 'en':
		content = "Stacking limitation"
	else:
		content = "Stacking limitation"
	return content


@register.simple_tag
def error_storage_email_error_10(lang=None):
	if lang == 'en':
		content = "Email Error"
	elif lang == 'tr':
		content = "Email Hatası"
	elif lang == 'tr':
		content = "Email Hatası"
	elif lang == 'tr':
		content = "Email Hatası"
	elif lang == 'ru':
		content = "Ошибка e-mail"
	else:
		content = "Email Error"
	return content


@register.simple_tag
def error_storage_warning_2(lang=None):
	if lang == 'en':
		content = "Warning"
	elif lang == 'tr':
		content = "Uyarı"
	else:
		content = "Warning"
	return content


@register.simple_tag
def error_storage_empty_error_1(lang=None):
	if lang == 'en':
		content = "Empty Error"
	else:
		content = "Empty Error"
	return content


@register.simple_tag
def error_storage_error_title(lang=None):
	if lang == 'en':
		content = "Error title"
	elif lang == 'tr':
		content = "Hata başlığı"
	else:
		content = "Error title"
	return content


@register.simple_tag
def error_storage_error_view(lang=None):
	if lang == 'en':
		content = "Error view"
	else:
		content = "Error view"
	return content


@register.simple_tag
def error_storage_error_solution(lang=None):
	if lang == 'en':
		content = "Error solution"
	elif lang == 'tr':
		content = "Hata çözümü"
	else:
		content = "Error solution"
	return content


@register.simple_tag
def error_storage_error_description(lang=None):
	if lang == 'en':
		content = "Error description"
	elif lang == 'tr':
		content = "Hata açıklaması"
	else:
		content = "Error description"
	return content


@register.simple_tag
def error_storage_error_group(lang=None):
	if lang == 'en':
		content = "Error group"
	elif lang == 'tr':
		content = "Hata grubu"
	else:
		content = "Error group"
	return content


@register.simple_tag
def error_storage_error_url(lang=None):
	if lang == 'en':
		content = "Error url"
	elif lang == 'tr':
		content = "Hata linki"
	else:
		content = "Error url"
	return content


@register.simple_tag
def error_storage_add_error(lang=None):
	if lang == 'en':
		content = "Add error"
	elif lang == 'tr':
		content = "Hata ekle"
	else:
		content = "Add error"
	return content


@register.simple_tag
def error_storage_add_error_group(lang=None):
	if lang == 'en':
		content = "Add error group"
	elif lang == 'tr':
		content = "Hata grubu ekle"
	else:
		content = "Add error group"
	return content


@register.simple_tag
def error_storage_error_group_title(lang=None):
	if lang == 'en':
		content = "Error group title"
	elif lang == 'tr':
		content = "Hata grubu başlığı"
	else:
		content = "Error group title"
	return content


@register.simple_tag
def error_storage_error_list(lang=None):
	if lang == 'en':
		content = "                                                               Errors list"
	elif lang == 'tr':
		content = "Hatalar listesi"
	else:
		content = "                                                               Errors list"
	return content


@register.simple_tag
def error_storage_error_groups(lang=None):
	if lang == 'en':
		content = "Error groups"
	elif lang == 'tr':
		content = "Hata grupları"
	else:
		content = "Error groups"
	return content


@register.simple_tag
def error_storage_error_code(lang=None):
	if lang == 'en':
		content = "Error code"
	elif lang == 'tr':
		content = "Hata kodu"
	else:
		content = "Error code"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	elif lang == 'tr':
		content = ""
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_notifications_list(lang=None):
	if lang == 'en':
		content = "Notifications list"
	elif lang == 'tr':
		content = "Bildirim listesi"
	else:
		content = "Notifications list"
	return content


@register.simple_tag
def notification_notification_title(lang=None):
	if lang == 'en':
		content = "Notification title"
	elif lang == 'tr':
		content = "Bildirim başlığı"
	else:
		content = "Notification title"
	return content


@register.simple_tag
def notification_notification_type(lang=None):
	if lang == 'en':
		content = "Notification type"
	elif lang == 'tr':
		content = "Bildirim türü"
	else:
		content = "Notification type"
	return content


@register.simple_tag
def notification_notification_group(lang=None):
	if lang == 'en':
		content = "Notification group"
	elif lang == 'tr':
		content = "Bildirim grubu"
	else:
		content = "Notification group"
	return content


@register.simple_tag
def notification_notification_description(lang=None):
	if lang == 'en':
		content = "Notification description"
	elif lang == 'tr':
		content = "Bildirim açıklaması"
	else:
		content = "Notification description"
	return content


@register.simple_tag
def notification_natification_groups(lang=None):
	if lang == 'en':
		content = "                                                               Notification groups"
	elif lang == 'tr':
		content = "Bildirim grupları"
	else:
		content = "                                                               Notification groups"
	return content


@register.simple_tag
def notification_notification_types(lang=None):
	if lang == 'en':
		content = "Notification types"
	elif lang == 'tr':
		content = "Bildirim türleri"
	else:
		content = "Notification types"
	return content


@register.simple_tag
def notification_add_notification(lang=None):
	if lang == 'en':
		content = "Add notification"
	elif lang == 'tr':
		content = "Bildirim ekle"
	else:
		content = "Add notification"
	return content


@register.simple_tag
def notification_add_notification_group(lang=None):
	if lang == 'en':
		content = "Add notification group"
	elif lang == 'tr':
		content = "Bildirim grubu ekle"
	else:
		content = "Add notification group"
	return content


@register.simple_tag
def notification_add_notification_type(lang=None):
	if lang == 'en':
		content = "Add notification type"
	elif lang == 'tr':
		content = "Bildirim türü ekle"
	else:
		content = "Add notification type"
	return content


@register.simple_tag
def notification_notification_group_title(lang=None):
	if lang == 'en':
		content = "Group title"
	elif lang == 'tr':
		content = "Grup başlığı"
	else:
		content = "Group title"
	return content


@register.simple_tag
def notification_notification_type_title(lang=None):
	if lang == 'en':
		content = "Type title"
	elif lang == 'tr':
		content = "Tür başlığı"
	else:
		content = "Type title"
	return content


@register.simple_tag
def error_storage_select_error_group(lang=None):
	if lang == 'en':
		content = "Select error group                                "
	elif lang == 'tr':
		content = "Hata grubu seçin"
	else:
		content = "Select error group                                "
	return content


@register.simple_tag
def notification_select_notification_type(lang=None):
	if lang == 'en':
		content = "Select notification type"
	elif lang == 'tr':
		content = "Bildirim türü seç"
	else:
		content = "Select notification type"
	return content


@register.simple_tag
def notification_select_notification_group(lang=None):
	if lang == 'en':
		content = "Select notification group"
	elif lang == 'tr':
		content = "Bildirim grubu seç"
	else:
		content = "Select notification group"
	return content


@register.simple_tag
def error_storage_issues_4(lang=None):
	if lang == 'en':
		content = "Issues"
	else:
		content = "Issues"
	return content


@register.simple_tag
def error_storage_tickets_2(lang=None):
	if lang == 'en':
		content = "Tickets"
	else:
		content = "Tickets"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_info_2(lang=None):
	if lang == 'en':
		content = "Info"
	else:
		content = "Info"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test2_1(lang=None):
	if lang == 'en':
		content = "Test2"
	else:
		content = "Test2"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_1(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_ornek_1(lang=None):
	if lang == 'en':
		content = "Ornek"
	else:
		content = "Ornek"
	return content


@register.simple_tag
def notification_testt_7(lang=None):
	if lang == 'en':
		content = "Testt"
	else:
		content = "Testt"
	return content


@register.simple_tag
def notification_test_7(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_7(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


@register.simple_tag
def notification_test_7(lang=None):
	if lang == 'en':
		content = "Test"
	else:
		content = "Test"
	return content


