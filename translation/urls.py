"""translation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from lingvo import views as lingvo_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', lingvo_views.homepage, name='homepage'),

    url(r'^translation/$', lingvo_views.translation_list),
    url(r'^translation/(\d+)/$', lingvo_views.translation_list),
    url(r'^translation/search-ajax/$', lingvo_views.translation_search_ajax),
    url(r'^translation/search-translated-ajax/$', lingvo_views.translation_search_translated_ajax),
    url(r'^translation/translation-add/$', lingvo_views.translation_add),
    url(r'^translation/change-show-count/$', lingvo_views.translation_change_show_count),
    url(r'^translation/change-show-group/$', lingvo_views.translation_change_show_group),
    url(r'^translation/change-show-untranslated/$', lingvo_views.translation_change_show_untranslated),

    url(r'^languages/$', lingvo_views.language_manager_list),
    url(r'^languages/language_add/$', lingvo_views.language_add),
    url(r'^languages/language_delete/$', lingvo_views.language_delete),

    url(r'^groups/$', lingvo_views.group_manager_list),
    url(r'^groups/group_add/$', lingvo_views.group_add),
    url(r'^groups/group_delete/$', lingvo_views.group_delete),

    url(r'^main_dictionary/$', lingvo_views.main_dictionary_list),
    url(r'^main_dictionary/(\d+)/$', lingvo_views.main_dictionary_list),
    url(r'^main_dictionary/change-show-count/$', lingvo_views.main_dictionary_change_show_count),
    url(r'^main_dictionary/add/$', lingvo_views.main_dictionary_add),

    url(r'^login/$', auth_views.login, name='login'),
    url(r'^login/submit/$', lingvo_views.user_login),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),

    url(r'^test/$', lingvo_views.test),

    url(r'^system-value-available/$', lingvo_views.system_value_available),
    url(r'^system-value-slugify/$', lingvo_views.slugify_function),
    
    url(r'post/$', lingvo_views.post),
    url(r'delete-access-group-item/$', lingvo_views.delete_access_group_item),
    url(r'add-access-item/$', lingvo_views.add_access_item),
    url(r'^delete-access-permission-item/$', lingvo_views.delete_access_item),
    url(r'^get-translations/$', lingvo_views.get_translations),
    url(r'^save-translations/$', lingvo_views.save_translations),
    url(r'^sync-json/$', lingvo_views.export_json),
    url(r'^get-translation-dictionary/$', lingvo_views.get_translation_dictionary_by_lang_code),
]
