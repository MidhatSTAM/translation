awesome-slugify==1.6.5
Django==1.11
Pillow==5.2.0
psycopg2==2.7.5
pytz==2018.5
regex==2018.8.29
six==1.11.0
Unidecode==0.4.21
